#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o ex2_3.txt

# argv[0]: execution path.
# argv[1]: memory size. Default memory size 256*sizeof(int), = 1kB.
# argv[2]: direction of memcpy. 0 device to host, 1 host to device.
# argv[3]: memory location pageable or pinned. 0 pageable memory, 1 pinned memory


# d2h: device to host
# h2d: host to device

# Type 1: run on d2h, pageable 
echo "Type 1: run on d2h, pageable...... "
# 1: 1kB, 
./memcpy 256 0 0

# 2: 100kB, d2h, pageable 
./memcpy 25600 0 0

# 3: 1MB, d2h, pageable 
./memcpy 262144 0 0

# 4: 10MB, d2h, pageable 
./memcpy 2621440 0 0

# 5: 50MB, d2h, pageable 
./memcpy 13107200 0 0

# 6: 100MB, d2h, pageable 
./memcpy 26214400 0 0

# 7: 200MB, d2h, pageable 
./memcpy 52428800 0 0

# 8: 500MB, d2h, pageable 
./memcpy 131072000 0 0

# 9: 800MB, d2h, pageable 
./memcpy 209715200 0 0

# 10: 1GB, d2h, pageable 
./memcpy 268435456 0 0



# Type 2: run on h2d, pageable 
echo "Type 2: run on h2d, pageable...... "
# 1: 1kB
./memcpy 256 1 0

# 2: 100kB
./memcpy 25600 1 0

# 3: 1MB
./memcpy 262144 1 0

# 4: 10MB
./memcpy 2621440 1 0

# 5: 50MB
./memcpy 13107200 1 0

# 6: 100MB
./memcpy 26214400 1 0

# 7: 200MB
./memcpy 52428800 1 0

# 8: 500MB 
./memcpy 131072000 1 0

# 9: 800MB
./memcpy 209715200 1 0

# 10: 1GB
./memcpy 268435456 1 0





# Type 3: run on d2h, pinned 
echo "Type 3: run on d2h, pinned...... "
# 1: 1kB, 
./memcpy 256 0 1

# 2: 100kB, d2h,  
./memcpy 25600 0 1

# 3: 1MB, d2h,  
./memcpy 262144 0 1

# 4: 10MB, d2h,  
./memcpy 2621440 0 1

# 5: 50MB, d2h,  
./memcpy 13107200 0 1

# 6: 100MB, d2h,  
./memcpy 26214400 0 1

# 7: 200MB, d2h,  
./memcpy 52428800 0 1

# 8: 500MB, d2h,  
./memcpy 131072000 0 1

# 9: 800MB, d2h,  
./memcpy 209715200 0 1

# 10: 1GB, d2h,  
./memcpy 268435456 0 1



# Type 4: run on h2d, pinned 
echo "Type 4: run on h2d, pinned...... "
# 1: 1kB
./memcpy 256 1 1

# 2: 100kB
./memcpy 25600 1 1

# 3: 1MB
./memcpy 262144 1 1

# 4: 10MB
./memcpy 2621440 1 1

# 5: 50MB
./memcpy 13107200 1 1

# 6: 100MB
./memcpy 26214400 1 1

# 7: 200MB
./memcpy 52428800 1 1

# 8: 500MB 
./memcpy 131072000 1 1

# 9: 800MB
./memcpy 209715200 1 1

# 10: 1GB
./memcpy 268435456 1 1


