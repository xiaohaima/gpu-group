#! /usr/bin/env bash
# ex01
cd ../
cd ex2.1
nvcc null_kernel.cu -o null_kernel
mv null_kernel ../bin
cd ../ex2.2
nvcc breakEvenKernel.cu -o breakEvenKernel
mv breakEvenKernel ../bin
cd ../ex2.3
nvcc memcpy.cu -o memcpy
mv memcpy ../bin