import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

data = pd.read_csv("ex2.3.csv", sep=",", decimal=".")

data_am = ["1kB", "100kB", "1MB", "10MB", "50MB", "100MB", "200MB", "500MB", "800MB", "1GB"]

print(data.head())
print(data.columns)
print(data["H2D-pageable"])
D2H_pageable = data["D2H-pageable"].to_numpy()
H2D_pageable = data["H2D-pageable"].to_numpy()
D2H_pinned = data["D2H-pinned"].to_numpy()
H2D_pinned = data["H2D-pinned"].to_numpy()
linestyles = ['-', ':', '--', '-.', 'None']
colors = ['r', 'g', 'b', 'y', 'magenta', 'None']

plt.figure(figsize=(12, 7))
x = range(len(data_am))
plt.xticks(x, data_am, rotation=45)
plt.plot(x, D2H_pageable, linestyle=linestyles[0], c=colors[0], label="D2H_pageable")
plt.plot(x, H2D_pageable, linestyle=linestyles[1], c=colors[1], label="H2D_pageable")
plt.plot(x, D2H_pinned, linestyle=linestyles[2], c=colors[2], label="D2H_pinned")
plt.plot(x, H2D_pinned, linestyle=linestyles[3], c=colors[3], label="H2D_pinned")

plt.title("GPU memcpy compare time costs, from 1KB-1GB, time unit: us")
plt.legend()
plt.show()

# 1KiB = 1GiB /(1024*1024)
factors = [1/(1024*1024), 100/(1024*1024), 1/1024, 10/1024, 50/1024, 100/1021, 200/1024, 500/1024, 800/1024, 1]

throughputs = {"D2H_pageable": [], "H2D_pageable": [], "D2H_pinned": [], "H2D_pinned": []}

for i, t in enumerate(D2H_pageable):
    throughput = (factors[i] * (1e6/t))
    throughputs["D2H_pageable"].append(throughput)

for i, t in enumerate(H2D_pageable):
    throughput = (factors[i] * (1e6/t))
    throughputs["H2D_pageable"].append(throughput)

for i, t in enumerate(D2H_pinned):
    throughput = (factors[i] * (1e6/t))
    throughputs["D2H_pinned"].append(throughput)

for i, t in enumerate(H2D_pinned):
    throughput = (factors[i] * (1e6/t))
    throughputs["H2D_pinned"].append(throughput)


plt.figure(figsize=(12, 7))
x = range(len(data_am))
plt.xticks(x, data_am, rotation=45)
plt.plot(x, throughputs["D2H_pageable"], linestyle=linestyles[0], c=colors[0], label="D2H_pageable")
plt.plot(x, throughputs["H2D_pageable"], linestyle=linestyles[1], c=colors[1], label="H2D_pageable")
plt.plot(x, throughputs["D2H_pinned"], linestyle=linestyles[2], c=colors[2], label="D2H_pinned")
plt.plot(x, throughputs["H2D_pinned"], linestyle=linestyles[3], c=colors[3], label="H2D_pinned")

plt.title("GPU memcpy compare throughput, memcpy from 1KB-1GB, throughput unit: GByte/s")
plt.legend()
plt.show()
