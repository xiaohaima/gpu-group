#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "chTimer.h"

void memcpy_pageable_dtoh(int N)
{
  chTimerTimestamp start, stop;
  void *hmem = malloc(N*sizeof(int));
  void *dmem = NULL;
  cudaMalloc((void**)&dmem, N*sizeof(int));
  chTimerGetTime(&start);
  cudaMemcpy(hmem, dmem, N*sizeof(int), cudaMemcpyDeviceToHost);
  chTimerGetTime(&stop);
  cudaFree(dmem); // Free device buffer
  free(hmem); // Free host buffer
  double us = chTimerElapsedTime(&start, &stop);
  double avg = (us * (double) 1e6);
  printf("%.2f\n", avg);
}

void memcpy_pageable_htod(int N)
{
  chTimerTimestamp start, stop;
  void *hmem = malloc(N*sizeof(int));
  void *dmem = NULL;
  cudaMalloc((void**)&dmem, N*sizeof(int));
  chTimerGetTime(&start);
  cudaMemcpy(dmem, hmem, N*sizeof(int), cudaMemcpyHostToDevice);
  chTimerGetTime(&stop);
  cudaFree(dmem); // Free device buffer
  free(hmem); // Free host buffer
  double us = chTimerElapsedTime(&start, &stop);
  double avg = (us * (double) 1e6);
  printf("%.2f\n", avg);
}

void memcpy_pinned_dtoh(int N)
{
  chTimerTimestamp start, stop;
  void *hmem = NULL;
  cudaMallocHost((void**)&hmem, N*sizeof(int));
  void *dmem = NULL;
  cudaMalloc((void**)&dmem, N*sizeof(int));
  chTimerGetTime(&start);
  cudaMemcpy(hmem, dmem, N*sizeof(int), cudaMemcpyDeviceToHost);
  chTimerGetTime(&stop);
  cudaFree(dmem); // Free device buffer
  cudaFreeHost(hmem); // Free host buffer
  double us = chTimerElapsedTime(&start, &stop);
  double avg = (us * (double) 1e6);
  printf("%.2f\n", avg);
}

void memcpy_pinned_htod(int N)
{

  chTimerTimestamp start, stop;
  void *hmem = NULL;
  cudaMallocHost((void**)&hmem, N*sizeof(int));
  void *dmem = NULL;
  cudaMalloc((void**)&dmem, N*sizeof(int));
  chTimerGetTime(&start);
  cudaMemcpy(dmem, hmem, N*sizeof(int), cudaMemcpyHostToDevice);
  chTimerGetTime(&stop);
  cudaFree(dmem); // Free device buffer
  cudaFreeHost(hmem); // Free host buffer
  double us = chTimerElapsedTime(&start, &stop);
  double avg = (us * (double) 1e6);
  printf("%.2f\n", avg);
}

// argv[0]: execution path, argv[1]: memory size, argv[2]: direction of memcpy, argv[3]: memory location pageable or pinned
/*
* argv[0]: execution path.
* argv[1]: memory size. Default memory size 256*sizeof(int), = 1kB.
* argv[2]: direction of memcpy. 0 device to host, 1 host to device.
* argv[3]: memory location pageable or pinned. 0 pageable memory, 1 pinned memory
*/
int main(int argc, char *argv[]){
  int N = 256;  // default memory size 256*sizeof(int), = 1kB.
  int dirt = 0; // 0 device to host, 1 host to device
  int mem_loc = 0; // 0 pageable memory, 1 pinned memory
  if (argc == 2){
    N = atoi(argv[1]);
  } else if (argc == 3){
    N = atoi(argv[1]);
	dirt = atoi(argv[2]);
  } else if (argc == 4){
    N = atoi(argv[1]);
	dirt = atoi(argv[2]);
	mem_loc = atoi(argv[3]);
  }
  printf("%d,", N*4/1024);
  if (dirt == 0 && mem_loc == 0){
    memcpy_pageable_dtoh(N);
  }
  if (dirt == 1 && mem_loc == 0){
    memcpy_pageable_htod(N);
  }
  if (dirt == 0 && mem_loc == 1){
    memcpy_pinned_dtoh(N);
  }
  if (dirt == 1 && mem_loc == 1){
    memcpy_pinned_htod(N);
  }
  return 0;
}