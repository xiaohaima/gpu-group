import csv
import numpy as np
import matplotlib.pyplot as plt

# Plot Setup
plt.xlabel("Cycles")
plt.ylabel("µs Time")
x = []
y = []
with open("ex2.2_out.csv", "r") as f:
    csv_data = csv.DictReader(f)
    for row in csv_data:
        x.append(float(row["cycles"]))
        y.append(float(row["usLaunch"]))
plt.plot(x,y)
plt.savefig("plot.png")