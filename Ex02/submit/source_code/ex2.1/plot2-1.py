import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import style

style.use('ggplot')

fig = plt.figure()
ax1 = fig.add_subplot(111, projection='3d')

dim_grid = np.array([1, 1820, 3640, 5460, 7280, 9100, 10920, 12740, 14560, 16384])
dim_block = np.array([1, 113, 226, 339, 452, 565, 678, 791, 904, 1024])
async_time = np.array([2.68, 2.46, 3.13, 6.74, 8.94, 20.98, 25.67, 30.58, 35.78, 40.93])
sync_time = np.array([6.28, 7.85, 8.18, 11.77, 13.97, 26.07, 30.81, 35.57, 40.74, 45.90])

ax1.scatter(dim_grid, dim_block, async_time, c='g', marker='o', label="async_time")
ax1.scatter(dim_grid, dim_block, sync_time, c ='r', marker='o', label="sync_time")

ax1.set_xlabel('dim_grid axis')
ax1.set_ylabel('dim_block axis')
ax1.set_zlabel('time axis')

plt.legend()
plt.show()
