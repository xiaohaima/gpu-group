#include <stdio.h>
#include <stdlib.h>

#include "chTimer.h"


int memcpy_pageable_dtoh(int N) {
    const int cIterations = 100;
    // printf("Measuring pageable memcpy time from device to host... "); 
    // fflush(stdout);

    chTimerTimestamp start, stop;

    void *hmem = malloc(N*sizeof(int));
    void *dmem = NULL;
    cudaMalloc((void**)&dmem, N*sizeof(int));

    chTimerGetTime(&start);
    for (int i = 0; i < cIterations; i++) {
        cudaMemcpy(hmem, dmem, N*sizeof(int), cudaMemcpyDeviceToHost);
        // According to documentation, transfers from device to either pageable or pinned host memory are synchronous
        // cudaDeviceSynchronize();
    }
    cudaDeviceSynchronize();
    chTimerGetTime(&stop);

    cudaFree(dmem); // Free device buffer
    free(hmem); // Free host buffer

    {
        double microseconds = 1e6 * chTimerElapsedTime(&start, &stop);
        double usPerLaunch = microseconds / (float)cIterations;

        printf("%.2f us\n", usPerLaunch);
    }

    return 0;
}


int memcpy_pageable_htod(int N) {
    const int cIterations = 100;
    // printf("Measuring pageable memcpy time from host to device... "); 
    // fflush(stdout);

    chTimerTimestamp start, stop;

    void *hmem = malloc(N*sizeof(int));
    void *dmem = NULL;
    cudaMalloc((void**)&dmem, N*sizeof(int));

    chTimerGetTime(&start);
    for (int i = 0; i < cIterations; i++) {
        cudaMemcpy(dmem, hmem, N*sizeof(int), cudaMemcpyHostToDevice);
        // According to documentation, transfers from pageable host memory to device memory, DMA may not have completed
        cudaDeviceSynchronize();
    }
    chTimerGetTime(&stop);

    cudaFree(dmem); // Free device buffer
    free(hmem); // Free host buffer

    {
        double microseconds = 1e6 * chTimerElapsedTime(&start, &stop);
        double usPerLaunch = microseconds / (float)cIterations;

        printf("%.2f us\n", usPerLaunch);
    }

  return 0;
}


int memcpy_pinned_dtoh(int N) {
    const int cIterations = 100;
    // printf("Measuring pinned memcpy time from device to host... "); 
    // fflush(stdout);

    chTimerTimestamp start, stop;

    void *hmem = NULL;
    cudaMallocHost((void**)&hmem, N*sizeof(int));
    void *dmem = NULL;
    cudaMalloc((void**)&dmem, N*sizeof(int));

    chTimerGetTime(&start);
    for (int i = 0; i < cIterations; i++) {
        cudaMemcpy(hmem, dmem, N*sizeof(int), cudaMemcpyDeviceToHost);
        // According to documentation, transfers from device to either pageable or pinned host memory are synchronous
        // cudaDeviceSynchronize();
    }
    cudaDeviceSynchronize();
    chTimerGetTime(&stop);

    cudaFree(dmem); // Free device buffer
    cudaFreeHost(hmem); // Free host buffer

    {
        double microseconds = 1e6 * chTimerElapsedTime(&start, &stop);
        double usPerLaunch = microseconds / (float)cIterations;

        printf("%.2f us\n", usPerLaunch);
    }

  return 0;
}


int memcpy_pinned_htod(int N) {
    const int cIterations = 100;
    // printf("Measuring pinned memcpy time from host to device... "); 
    // fflush(stdout);

    chTimerTimestamp start, stop;

    void *hmem = NULL;
    cudaMallocHost((void**)&hmem, N*sizeof(int));
    void *dmem = NULL;
    cudaMalloc((void**)&dmem, N*sizeof(int));

    chTimerGetTime(&start);
    for (int i = 0; i < cIterations; i++) {
        cudaMemcpy(dmem, hmem, N*sizeof(int), cudaMemcpyHostToDevice);
        // According to documentation, transfers from pinned host memory to device memory are synchronous
        // cudaDeviceSynchronize();
    }
    cudaDeviceSynchronize();
    chTimerGetTime(&stop);

    cudaFree(dmem); // Free device buffer
    cudaFreeHost(hmem); // Free host buffer

    {
        double microseconds = 1e6 * chTimerElapsedTime(&start, &stop);
        double usPerLaunch = microseconds / (float)cIterations;

        printf("%.2f us\n", usPerLaunch);
    }

  return 0;
}


int memcpy_dtod(int N) {
    const int cIterations = 100;
    // printf("Measuring memcpy time from device to device... "); 
    // fflush(stdout);

    chTimerTimestamp start, stop;

    void *dmem1 = NULL;
    cudaMalloc((void**)&dmem1, N*sizeof(int));
    void *dmem2 = NULL;
    cudaMalloc((void**)&dmem2, N*sizeof(int));

    chTimerGetTime(&start);
    for (int i = 0; i < cIterations; i++) {
        cudaMemcpy(dmem2, dmem1, N*sizeof(int), cudaMemcpyDeviceToDevice);
        // According to documentation, transfers from device memory to device memory are asynchronous
        cudaDeviceSynchronize();
    }
    chTimerGetTime(&stop);

    cudaFree(dmem2); // Free device buffer
    cudaFree(dmem1); // Free device buffer

    {
        double microseconds = 1e6 * chTimerElapsedTime(&start, &stop);
        double usPerLaunch = microseconds / (float)cIterations;

        printf("%.2f us\n", usPerLaunch);
    }

  return 0;
}


// argv[0]: execution path, argv[1]: memory size, argv[2]: direction of memcpy, argv[3]: memory location pageable or pinned
/*
* argv[0]: execution path.
* argv[1]: memory size. Default memory size 256*sizeof(int), = 1kB.
* argv[2]: direction of memcpy. 0 device to host, 1 host to device.
* argv[3]: memory location pageable or pinned. 0 pageable memory, 1 pinned memory
* special case dtod: dirt = 2, mem_loc = 2
*/
int main(int argc, char *argv[]) {
    int N = 256;  // default memory size 256*sizeof(int), = 1kB.
    int dirt = 0; // 0 device to host, 1 host to device
    int mem_loc = 0; // 0 pageable memory, 1 pinned memory
    // Add dtod: dirt = 2, mem_loc = 2

    N = atoi(argv[1]);
	dirt = atoi(argv[2]);
	mem_loc = atoi(argv[3]);
    printf("%d,", N*4/1024);

    if (dirt == 0 && mem_loc == 0){
        memcpy_pageable_dtoh(N);
    }
    if (dirt == 1 && mem_loc == 0){
        memcpy_pageable_htod(N);
    }
    if (dirt == 0 && mem_loc == 1){
        memcpy_pinned_dtoh(N);
    }
    if (dirt == 1 && mem_loc == 1){
        memcpy_pinned_htod(N);
    }
    if (dirt == 2 && mem_loc == 2){
        memcpy_dtod(N);
    }

    return 0;
}