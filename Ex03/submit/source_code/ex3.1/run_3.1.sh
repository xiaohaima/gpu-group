#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o ex3_1.csv

# argv[0]: execution path.
# argv[1]: memory size. Default memory size 256*sizeof(int), = 1kB.
# argv[2]: direction of memcpy. 0 device to host, 1 host to device.
# argv[3]: memory location pageable or pinned. 0 pageable memory, 1 pinned memory
# special case dtod: dirt = 2, mem_loc = 2


# d2h: device to host
# h2d: host to device
# d2d: device to device

# Type 1: run on d2h, pageable 
echo "Type 1: run on d2h, pageable...... "
# 1: 1kB, 
./cudamemcpy 256 0 0

# 2: 100kB, d2h, pageable 
./cudamemcpy 25600 0 0

# 3: 1MB, d2h, pageable 
./cudamemcpy 262144 0 0

# 4: 10MB, d2h, pageable 
./cudamemcpy 2621440 0 0

# 5: 50MB, d2h, pageable 
./cudamemcpy 13107200 0 0

# 6: 100MB, d2h, pageable 
./cudamemcpy 26214400 0 0

# 7: 200MB, d2h, pageable 
./cudamemcpy 52428800 0 0

# 8: 500MB, d2h, pageable 
./cudamemcpy 131072000 0 0

# 9: 800MB, d2h, pageable 
./cudamemcpy 209715200 0 0

# 10: 1GB, d2h, pageable 
./cudamemcpy 268435456 0 0



# Type 2: run on h2d, pageable 
echo "Type 2: run on h2d, pageable...... "
# 1: 1kB
./cudamemcpy 256 1 0

# 2: 100kB
./cudamemcpy 25600 1 0

# 3: 1MB
./cudamemcpy 262144 1 0

# 4: 10MB
./cudamemcpy 2621440 1 0

# 5: 50MB
./cudamemcpy 13107200 1 0

# 6: 100MB
./cudamemcpy 26214400 1 0

# 7: 200MB
./cudamemcpy 52428800 1 0

# 8: 500MB 
./cudamemcpy 131072000 1 0

# 9: 800MB
./cudamemcpy 209715200 1 0

# 10: 1GB
./cudamemcpy 268435456 1 0





# Type 3: run on d2h, pinned 
echo "Type 3: run on d2h, pinned...... "
# 1: 1kB, 
./cudamemcpy 256 0 1

# 2: 100kB, d2h,  
./cudamemcpy 25600 0 1

# 3: 1MB, d2h,  
./cudamemcpy 262144 0 1

# 4: 10MB, d2h,  
./cudamemcpy 2621440 0 1

# 5: 50MB, d2h,  
./cudamemcpy 13107200 0 1

# 6: 100MB, d2h,  
./cudamemcpy 26214400 0 1

# 7: 200MB, d2h,  
./cudamemcpy 52428800 0 1

# 8: 500MB, d2h,  
./cudamemcpy 131072000 0 1

# 9: 800MB, d2h,  
./cudamemcpy 209715200 0 1

# 10: 1GB, d2h,  
./cudamemcpy 268435456 0 1



# Type 4: run on h2d, pinned 
echo "Type 4: run on h2d, pinned...... "
# 1: 1kB
./cudamemcpy 256 1 1

# 2: 100kB
./cudamemcpy 25600 1 1

# 3: 1MB
./cudamemcpy 262144 1 1

# 4: 10MB
./cudamemcpy 2621440 1 1

# 5: 50MB
./cudamemcpy 13107200 1 1

# 6: 100MB
./cudamemcpy 26214400 1 1

# 7: 200MB
./cudamemcpy 52428800 1 1

# 8: 500MB 
./cudamemcpy 131072000 1 1

# 9: 800MB
./cudamemcpy 209715200 1 1

# 10: 1GB
./cudamemcpy 268435456 1 1



# Type 5: run on d2d
echo "Type 5: run on d2d...... "
# 1: 1kB
./cudamemcpy 256 2 2

# 2: 100kB
./cudamemcpy 25600 2 2

# 3: 1MB
./cudamemcpy 262144 2 2

# 4: 10MB
./cudamemcpy 2621440 2 2

# 5: 50MB
./cudamemcpy 13107200 2 2

# 6: 100MB
./cudamemcpy 26214400 2 2

# 7: 200MB
./cudamemcpy 52428800 2 2

# 8: 500MB 
./cudamemcpy 131072000 2 2

# 9: 800MB
./cudamemcpy 209715200 2 2

# 10: 1GB
./cudamemcpy 268435456 2 2





