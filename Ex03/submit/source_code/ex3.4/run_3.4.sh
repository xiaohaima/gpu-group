#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o ex3_4.txt

echo "start ......"


size=$((1024))
sizeofint=4

offset0=0
grid_dim0=64
block_dim0=$((($size - $sizeofint*$offset0) / $grid_dim0))
echo "block dim 0: $block_dim0"

offset1=1
grid_dim1=4
block_dim1=$((($size - $sizeofint*$offset1) / $grid_dim1))
echo "block dim 1: $block_dim1"

offset2=2
grid_dim2=127
block_dim2=$((($size - $sizeofint*$offset2) / $grid_dim2))
echo "block dim 2: $block_dim2"

offset4=4
grid_dim4=16
block_dim4=$((($size - $sizeofint*$offset4) / $grid_dim4))
echo "block dim 4: $block_dim4"

offset8=8
grid_dim8=16
block_dim8=$((($size - $sizeofint*$offset8) / $grid_dim8))
echo "block dim 8: $block_dim8"

offset16=16
grid_dim16=32
block_dim16=$((($size - $sizeofint*$offset16) / $grid_dim16))
echo "block dim 16: $block_dim16"

offset32=32
grid_dim32=16
block_dim32=$((($size - $sizeofint*$offset32) / $grid_dim32))
echo "block dim 32: $block_dim32"




echo "offset 0 >>>>>>>"
./bin/memCpy --global-offset --offset $offset0 -t $block_dim0 -g $grid_dim0

echo "offset 1 >>>>>>>"
./bin/memCpy --global-offset --offset $offset1 -t $block_dim1 -g $grid_dim1

echo "offset 2 >>>>>>>"
./bin/memCpy --global-offset --offset $offset2 -t $block_dim2 -g $grid_dim2

echo "offset 4 >>>>>>>"
./bin/memCpy --global-offset --offset $offset4 -t $block_dim4 -g $grid_dim4

echo "offset 8 >>>>>>>"
./bin/memCpy --global-offset --offset $offset8 -t $block_dim8 -g $grid_dim8

echo "offset 16 >>>>>>>"
./bin/memCpy --global-offset --offset $offset16 -t $block_dim16 -g $grid_dim16

echo "offset 32 >>>>>>>"
./bin/memCpy --global-offset --offset $offset32 -t $block_dim32 -g $grid_dim32


echo "end ......"





