import matplotlib.pyplot as plt
import numpy as np

offset = np.array([0, 1, 2, 4, 8, 16, 32])
bandwidth = np.array([1.55, 1.55, 1.54, 1.53, 1.50, 1.45, 1.36])


plt.figure(figsize=(12, 7))
plt.plot(offset, bandwidth, color="b")
plt.title("offset and throughput, memory size 102400, unit: GB/s")
plt.show()


