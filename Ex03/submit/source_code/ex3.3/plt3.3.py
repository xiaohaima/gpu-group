import matplotlib.pyplot as plt
import numpy as np

stride = np.array([1, 2, 4, 8, 16, 32])
bandwidth = np.array([39.56, 19.52, 9.80, 4.95, 2.46, 1.22])


plt.figure(figsize=(12, 7))
plt.plot(stride, bandwidth, color="b")
plt.xlabel("stride")
plt.ylabel("throughput")
plt.title("Stride and throughput, memory size 102400, unit: GB/s")
plt.show()


