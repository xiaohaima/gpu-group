/*************************************************************************************************
 *
 *        Computer Engineering Group, Heidelberg University - GPU Computing Exercise 03
 *
 *                           Group : 09
 *
 *                            File : main.cu
 *
 *                         Purpose : Memory Operations Benchmark
 *
 *************************************************************************************************/

//
// Kernels
//

__global__ void 
globalMemCoalescedKernel(int* out_data, int* in_data, int size /*TODO Parameters*/)
{
    /*TODO Kernel Code*/
    int step_size = gridDim.x * blockDim.x;
    int arr_size = size / sizeof size;
    int loops = arr_size / step_size;
    int x_idx = blockIdx.x * blockDim.x + threadIdx.x;
    if (loops * step_size + x_idx < arr_size) {
      loops += 1;
    }
    
    for (int k = 0; k < loops; k ++ ) {
      int jump = k * step_size;
      int idx = jump + x_idx;
      out_data[idx] = in_data[idx];
    }

}

void 
globalMemCoalescedKernel_Wrapper(dim3 gridDim, dim3 blockDim, int* out_data, int* in_data, int size /*TODO Parameters*/) {
  globalMemCoalescedKernel<<< gridDim, blockDim, 0 /*Shared Memory Size*/ >>>(out_data, in_data, size/*TODO Parameters*/);
}

__global__ void 
globalMemStrideKernel(int* out_data, int* in_data, int stride /*TODO Parameters*/)
{
    /*TODO Kernel Code*/
    int x_idx = (blockIdx.x * blockDim.x + threadIdx.x) * stride;
    out_data[x_idx] = in_data[x_idx];
}

void 
globalMemStrideKernel_Wrapper(dim3 gridDim, dim3 blockDim, int* out_data, int* in_data, int stride /*TODO Parameters*/) {
	globalMemStrideKernel<<< gridDim, blockDim, 0 /*Shared Memory Size*/ >>>(out_data, in_data, stride /*TODO Parameters*/);
}

__global__ void 
globalMemOffsetKernel(int* out_data, int* in_data, int offset /*TODO Parameters*/)
{
    /*TODO Kernel Code*/
    int x_idx = blockIdx.x * blockDim.x + threadIdx.x + offset;
    out_data[x_idx] = in_data[x_idx];
}

void 
globalMemOffsetKernel_Wrapper(dim3 gridDim, dim3 blockDim, int* out_data, int* in_data, int offset /*TODO Parameters*/) {
	globalMemOffsetKernel<<< gridDim, blockDim, 0 /*Shared Memory Size*/ >>>(out_data, in_data, offset /*TODO Parameters*/);
}

