#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o ex3_3.txt

echo "start ......"


size=$((100*1024))
sizeofint=4


stride1=1
grid_dim1=128
block_dim1=$(($size / $grid_dim1 / $stride1 / $sizeofint))
echo "block dim 1: $block_dim1"

stride2=2
grid_dim2=128
block_dim2=$(($size / $grid_dim2 / $stride2 / $sizeofint))
echo "block dim 2: $block_dim2"

stride4=4
grid_dim4=64
block_dim4=$(($size / $grid_dim4 / $stride4 / $sizeofint))
echo "block dim 4: $block_dim4"

stride8=8
grid_dim8=64
block_dim8=$(($size / $grid_dim8 / $stride8 / $sizeofint))
echo "block dim 8: $block_dim8"

stride16=16
grid_dim16=32
block_dim16=$(($size / $grid_dim16 / $stride16 / $sizeofint))
echo "block dim 16: $block_dim16"

stride32=32
grid_dim32=32
block_dim32=$(($size / $grid_dim32 / $stride32 / $sizeofint))
echo "block dim 32: $block_dim32"





echo "stride 1 >>>>>>>"
./bin/memCpy --global-stride --stride $stride1 -t $block_dim1 -g $grid_dim1

echo "stride 2 >>>>>>>"
./bin/memCpy --global-stride --stride $stride2 -t $block_dim2 -g $grid_dim2

echo "stride 4 >>>>>>>"
./bin/memCpy --global-stride --stride $stride4 -t $block_dim4 -g $grid_dim4

echo "stride 8 >>>>>>>"
./bin/memCpy --global-stride --stride $stride8 -t $block_dim8 -g $grid_dim8

echo "stride 16 >>>>>>>"
./bin/memCpy --global-stride --stride $stride16 -t $block_dim16 -g $grid_dim16

echo "stride 32 >>>>>>>"
./bin/memCpy --global-stride --stride $stride32 -t $block_dim32 -g $grid_dim32


echo "end ......"





