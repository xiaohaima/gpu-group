#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o ex3.2_test.txt

echo "start ......"

grid_dim=$1

block_dim=$2

itr=$3

size1=1024
size2=$((100*1024))
size3=$((1024*1024))
size4=$((10*1024*1024))
size5=$((50*1024*1024))
size6=$((100*1024*1024))
size7=$((200*1024*1024))
size8=$((500*1024*1024))
size9=$((800*1024*1024))
size10=$((1024*1024*1024))

echo "grid_dim = $grid_dim" 
echo "block_dim = $block_dim" 
echo "itr = $itr" 
# echo "size10 = $size10" 

# 1: 1kB
echo "1: 1kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --global-coalesced -s $size1 -t $block_dim -g $grid_dim -i $itr

# 2: 100kB
echo "2: 100kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --global-coalesced -s $size2 -t $block_dim -g $grid_dim -i $itr

# 3: 1MB
echo "3: 1MB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --global-coalesced -s $size3 -t $block_dim -g $grid_dim -i $itr

# 4: 10MB
echo "4: 10MB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --global-coalesced -s $size4 -t $block_dim -g $grid_dim -i $itr

# 5: 50MB
echo "5: 50MB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --global-coalesced -s $size5 -t $block_dim -g $grid_dim -i $itr

# 6: 100MB
echo "6: 100MB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --global-coalesced -s $size6 -t $block_dim -g $grid_dim -i $itr

# 7: 200MB
echo "7: 200MB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --global-coalesced -s $size7 -t $block_dim -g $grid_dim -i $itr

# 8: 500MB 
echo "8: 500MB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --global-coalesced -s $size8 -t $block_dim -g $grid_dim -i $itr

# 9: 800MB
echo "9: 800MB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --global-coalesced -s $size9 -t $block_dim -g $grid_dim -i $itr

# 10: 1GB
echo "10: 1GB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --global-coalesced -s $size10 -t $block_dim -g $grid_dim -i $itr
# ./bin/memCpy --global-coalesced -s 1073741824  -t 1024 -g 32




echo "end ......"

