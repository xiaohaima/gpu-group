#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o ex3.2_test.txt

echo "start ......"

grid_dim=$1

block_dim=$2

itr=$3

size8=$((500*1024*1024))


echo "grid_dim = $grid_dim" 
echo "block_dim = $block_dim" 
echo "itr = $itr" 
echo "total dim = $(($block_dim*$grid_dim))" 


# 8: 500MB 
echo "test opt on 500MB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --global-coalesced -s $size8 -t $block_dim -g $grid_dim -i $itr





echo "end ......"

