import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

data = pd.read_csv("ex3.2-block.csv", sep=",", decimal=".")

data_am = ["1kB", "100kB", "1MB", "10MB", "50MB", "100MB", "200MB", "500MB", "800MB", "1GB"]

print(data.head())
print(data.columns)
line1 = data["1"].to_numpy()
line2 = data["64"].to_numpy()
line3 = data["128"].to_numpy()
line4 = data["256"].to_numpy()
line5 = data["512"].to_numpy()
line6 = data["768"].to_numpy()
line7 = data["1024"].to_numpy()
linestyles = [':', ':', ':', ':', '--', '-.', '-', 'None']
colors = ['brown', 'r', 'g', 'b', 'y', 'magenta', 'c', 'None']

plt.figure(figsize=(12, 7))
x = range(len(data_am))
plt.xticks(x, data_am, rotation=45)
plt.plot(line1, linestyle=linestyles[0], c=colors[0], label="block_dim = 1")
plt.plot(line2, linestyle=linestyles[1], c=colors[1], label="block_dim = 64")
plt.plot(line3, linestyle=linestyles[2], c=colors[2], label="block_dim = 128")
plt.plot(line4, linestyle=linestyles[3], c=colors[3], label="block_dim = 256")
plt.plot(line5, linestyle=linestyles[4], c=colors[4], label="block_dim = 512")
plt.plot(line6, linestyle=linestyles[5], c=colors[5], label="block_dim = 768")
plt.plot(line7, linestyle=linestyles[6], c=colors[6], label="block_dim = 1024")

plt.title("GPU Global Memory Tests for block_dim change, from 1KB-1GB, unit: GB/s")
plt.legend()
plt.show()


inc_line = [line1[8], line2[8], line3[8], line4[8], line5[8], line6[8], line7[8]]
b_dim = [1, 64, 128, 256, 512, 768, 1024]
plt.figure(figsize=(12, 7))
# x = range(len(b_dim))
# plt.xticks(x, b_dim)
plt.plot(b_dim, inc_line, linestyle=linestyles[6], c=colors[6], label="size=800MB, grid_dim=1")
plt.title("GPU Global Memory Tests for block_dim change, for 800MB trans, unit: GB/s")
plt.legend()
plt.show()

# plt grid dim
data = pd.read_csv("ex3.2-grid.csv", sep=",", decimal=".")

print(data.head())
print(data.columns)
line1 = data["1"].to_numpy()
line2 = data["4"].to_numpy()
line3 = data["8"].to_numpy()
line4 = data["12"].to_numpy()
line5 = data["16"].to_numpy()
line6 = data["24"].to_numpy()
line7 = data["32"].to_numpy()
linestyles = [':', ':', ':', ':', '--', '-.', '-', 'None']
colors = ['brown', 'r', 'g', 'b', 'y', 'magenta', 'c', 'None']

plt.figure(figsize=(12, 7))
x = range(len(data_am))
plt.xticks(x, data_am, rotation=45)
plt.plot(line1, linestyle=linestyles[0], c=colors[0], label="grid_dim = 1")
plt.plot(line2, linestyle=linestyles[1], c=colors[1], label="grid_dim = 4")
plt.plot(line3, linestyle=linestyles[2], c=colors[2], label="grid_dim = 8")
plt.plot(line4, linestyle=linestyles[3], c=colors[3], label="grid_dim = 12")
plt.plot(line5, linestyle=linestyles[4], c=colors[4], label="grid_dim = 16")
plt.plot(line6, linestyle=linestyles[5], c=colors[5], label="grid_dim = 24")
plt.plot(line7, linestyle=linestyles[6], c=colors[6], label="grid_dim = 32")

plt.title("GPU Global Memory Tests for grid_dim change, from 1KB-1GB, unit: GB/s")
plt.legend()
plt.show()

inc_line = [line1[8], line2[8], line3[8], line4[8], line5[8], line6[8], line7[8]]
g_dim = [1, 4, 8, 12, 16, 24, 32]
plt.figure(figsize=(12, 7))
# x = range(len(g_dim))
# plt.xticks(x, g_dim)
plt.plot(g_dim, inc_line, linestyle=linestyles[6], c=colors[6], label="size=800MB, block_dim=1024")
plt.title("GPU Global Memory Tests for block_dim change, for 800MB trans, unit: GB/s")
plt.legend()
plt.show()


data = pd.read_csv("ex3.2-opt.csv", sep=",", decimal=".")
print(data.head())
print(data.columns)

line = data["th"].to_numpy()
plt.figure(figsize=(12, 7))
x = range(len(line))
plt.xticks(x, data["size"], rotation=45)
plt.plot(line, linestyle='-', c='c')

plt.title("GPU Global Memory Tests optimization, for 500MB trans, unit: GB/s")
plt.legend()
plt.show()
