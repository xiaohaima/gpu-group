#include <vector>
#include <iostream>
#include <random>
#include <ctime>
#include <algorithm>
#include <iterator>
#include "chTimer.hpp"


float cpu_sum(std::vector<float>& vec) {
    int size = vec.size();
    float vec_sum = 0;
    for(int i = 0; i < size; i++) {
        vec_sum += vec[i];
    }

    return vec_sum;
}

void bandwidth_test(int size, int itr=20) {
    float total_bandwidth = 0.0;
    float vec_sum = 0;
    float avg_bandwidth;

    for(int i = 0; i < itr; i++) {
        std::vector<float> vec(size, 1.0);

        ChTimer cpuTimer;
        cpuTimer.start();
        vec_sum = cpu_sum(vec);
        cpuTimer.stop();

        total_bandwidth += 1e-9 * cpuTimer.getBandwidth(size * sizeof(float));
    }
    avg_bandwidth = total_bandwidth / itr;

    // chTimerBandwidth() computes the bandwidth (bytes/s) given two timestamps
    std::cout << "bw = " << avg_bandwidth << "GB/s" << ", with memory size " << size * sizeof(float) << "B, " << std::endl 
    << "and sum = " << vec_sum << std::endl;

}

int main(int argc, char ** argv) {
    int itr = 20;
    if (argc == 3){
        itr = atoi(argv[2]);
    }

    auto size = atoi(argv[1]);
    bandwidth_test(size, itr);

    return 0;
}