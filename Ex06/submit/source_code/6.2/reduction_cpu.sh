#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o results.txt


size1=$((1024/4)) # 1kB
size2=$((2*1024/4)) # 2kB
size3=$((4*1024/4)) # 4kB
size4=$((8*1024/4)) # 8kB
size5=$((16*1024/4)) # 16kB
size6=$((20*1024/4)) # 24kB
size7=$((32*1024/4)) # 32kB
size8=$((36*1024/4)) # 36kB
size9=$((40*1024/4)) # 40kB
size10=$((48*1024/4)) # 48kB


echo "start.........................."


echo "1: 1kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction_cpu $size1

echo "2: 2kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction_cpu $size2

echo "3: 4kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction_cpu $size3

echo "4: 8kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction_cpu $size4

echo "5: 16kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction_cpu $size5

echo "6: 24kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction_cpu $size6

echo "7: 32kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction_cpu $size7

echo "8: 36kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction_cpu $size8

echo "9: 40kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction_cpu $size9

echo "10: 48kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction_cpu $size10


echo "end.........................."