start..........................
1: 1kB >>>>>>>>>>>>>>>>>>>>>>
bw = 1.86871GB/s, with memory size 1024B, 
and sum = 256
2: 2kB >>>>>>>>>>>>>>>>>>>>>>
bw = 1.93035GB/s, with memory size 2048B, 
and sum = 512
3: 4kB >>>>>>>>>>>>>>>>>>>>>>
bw = 1.96471GB/s, with memory size 4096B, 
and sum = 1024
4: 8kB >>>>>>>>>>>>>>>>>>>>>>
bw = 1.98237GB/s, with memory size 8192B, 
and sum = 2048
5: 16kB >>>>>>>>>>>>>>>>>>>>>>
bw = 1.99065GB/s, with memory size 16384B, 
and sum = 4096
6: 24kB >>>>>>>>>>>>>>>>>>>>>>
bw = 1.99297GB/s, with memory size 20480B, 
and sum = 5120
7: 32kB >>>>>>>>>>>>>>>>>>>>>>
bw = 1.99485GB/s, with memory size 32768B, 
and sum = 8192
8: 36kB >>>>>>>>>>>>>>>>>>>>>>
bw = 1.99563GB/s, with memory size 36864B, 
and sum = 9216
9: 40kB >>>>>>>>>>>>>>>>>>>>>>
bw = 1.99574GB/s, with memory size 40960B, 
and sum = 10240
10: 48kB >>>>>>>>>>>>>>>>>>>>>>
bw = 1.97037GB/s, with memory size 49152B, 
and sum = 12288
end..........................
