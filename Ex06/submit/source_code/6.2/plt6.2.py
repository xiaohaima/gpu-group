import matplotlib.pyplot as plt
import numpy as np


data_am = np.array(["1", "2", "4", "8", "16", "24", "32", "36", "40", "48"])

line = np.array([1.86871, 1.93035, 1.96471, 1.98237, 1.99065, 1.99297, 1.99485, 1.99563, 1.99574, 1.97037])
# line = np.log(line)


plt.figure(figsize=(12, 7))

plt.plot(data_am, line, ":")
plt.xlabel("array size, unit: kB")
plt.ylabel("bandwidth, unit: GB/s")

plt.title("reduction: cpu sequential version, vary the array size, unit: GB/s")
# plt.legend()
plt.show()
