/**************************************************************************************************
 *
 *       Computer Engineering Group, Heidelberg University - GPU Computing Exercise 06
 *
 *                 Gruppe : TODO
 *
 *                   File : kernel.cu
 *
 *                Purpose : Reduction
 *
 **************************************************************************************************/

#include <thrust/reduce.h>
#include <thrust/device_ptr.h>

//
// Reduction_Kernel
//
__global__ void
reduction_Kernel(int numElements, float* dataIn, float* dataOut)
{
  //extern __shared__ float sPartials[1024];
  extern __shared__ float sPartials[];
  const int tid = threadIdx.x;
	int elementId = blockIdx.x * blockDim.x + threadIdx.x;
	
	if (elementId < numElements)
	{
		/*TODO Kernel Code*/
		/* add additional for loops if requrired, e.g., if the max. grid size is not sufficient for the problem size */
    // do reduction in shared mem
    sPartials[tid] = dataIn[elementId];
    __syncthreads();
    for ( unsigned int s = 1; s < blockDim.x; s *= 2 ) {
      if ( tid % ( 2 * s ) == 0 ) {
        sPartials[tid] += sPartials[tid + s];
      }
      __syncthreads();
    }
    if ( tid == 0 ) {
      //*dataOut += sPartials[0];
      //dataOut[blockIdx.x] = sPartials[0]; 
      // Use dataIn store per partial sums in per block.
      dataIn[blockIdx.x * blockDim.x] = sPartials[0];
    }
    
    //if ( tid == 0 && blockIdx.x == 0) {

    //}
	}

}

__global__ void
total_sums_Kernel(int gridsize, int blocksize, float* dataIn, float* dataOut){
  for(unsigned int i = 0; i < gridsize; i++){
    *dataOut += dataIn[i * blocksize];
  }
}
void reduction_Kernel_Wrapper(dim3 gridSize, dim3 blockSize, int numElements, float* dataIn, float* dataOut) {
  //float* d_block_sums;
	//cudaMalloc(&d_block_sums, sizeof(*dataOut) * grid_sz.x);
	//cudaMemset(d_block_sums, 0, sizeof(*dataOut) * grid_sz.x);
	reduction_Kernel<<< gridSize, blockSize, sizeof(*dataOut)*blockSize.x >>>(numElements, dataIn, dataOut);
  //reduction_Kernel<<< gridSize, blockSize, sizeof(*dataOut)*blockSize.x >>>(numElements, dataIn, d_block_sums);
 	total_sums_Kernel<<< 1, 1>>>(gridSize.x, blockSize.x, dataIn, dataOut);
}

//
// Reduction Kernel using CUDA Thrust
//

void thrust_reduction_Wrapper(int numElements, float* dataIn, float* dataOut) {
	thrust::device_ptr<float> in_ptr = thrust::device_pointer_cast(dataIn);
	thrust::device_ptr<float> out_ptr = thrust::device_pointer_cast(dataOut);
	
	*out_ptr = thrust::reduce(in_ptr, in_ptr + numElements, (float) 0., thrust::plus<float>());	
}
