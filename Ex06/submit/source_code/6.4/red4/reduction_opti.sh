#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o results_opti.txt


size1=$((1024 / 4)) # 1kB
size2=$((2*1024 / 4)) # 2kB
size3=$((4*1024 / 4)) # 4kB
size4=$((8*1024 / 4)) # 8kB
size5=$((16*1024 / 4)) # 16kB
size6=$((20*1024 / 4)) # 24kB
size7=$((32*1024 / 4)) # 32kB
size8=$((36*1024 / 4)) # 36kB
size9=$((40*1024 / 4)) # 40kB
size10=$((48*1024 / 4)) # 48kB



block_dim3=256
block_dim5=32



echo "start.........................."


echo "block_dim = 256"
echo "1: 1kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size1 -t $block_dim3
echo "2: 2kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size2 -t $block_dim3
echo "3: 4kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size3 -t $block_dim3
echo "4: 8kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size4 -t $block_dim3
echo "5: 16kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size5 -t $block_dim3
echo "6: 24kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size6 -t $block_dim3
echo "7: 32kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size7 -t $block_dim3
echo "8: 36kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size8 -t $block_dim3
echo "9: 40kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size9 -t $block_dim3
echo "10: 48kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size10 -t $block_dim3


echo "block_dim = 32"
echo "1: 1kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size1 -t $block_dim5
echo "2: 2kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size2 -t $block_dim5
echo "3: 4kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size3 -t $block_dim5
echo "4: 8kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size4 -t $block_dim5
echo "5: 16kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size5 -t $block_dim5
echo "6: 24kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size6 -t $block_dim5
echo "7: 32kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size7 -t $block_dim5
echo "8: 36kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size8 -t $block_dim5
echo "9: 40kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size9 -t $block_dim5
echo "10: 48kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size10 -t $block_dim5



echo "end.........................."