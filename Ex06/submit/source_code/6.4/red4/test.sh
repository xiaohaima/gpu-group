#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o test.txt


size1=$((1024  / 4)) # 1kB
size2=$((2*1024 / 4)) # 2kB
size3=$((4*1024 / 4)) # 4kB
size4=$((8*1024 / 4)) # 8kB
size5=$((16*1024 / 4)) # 16kB
size6=$((20*1024 / 4)) # 24kB
size7=$((32*1024 / 4)) # 32kB
size8=$((36*1024 / 4)) # 36kB
size9=$((40*1024 / 4)) # 40kB
size10=$((48*1024 / 4)) # 48kB



echo "start.........................."


echo "1: 1kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size1 -t 256
echo "2: 2kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size2 -t 256
echo "3: 4kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size3 -t 256
echo "4: 8kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size4 -t 256
echo "5: 16kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size5 -t 256
echo "6: 24kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size6 -t 256
echo "7: 32kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size7 -t 256
echo "8: 36kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size8 -t 256
echo "9: 40kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size9 -t 256
echo "10: 48kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -s $size10 -t 256