start ......
size: '=': No such file
size: '1048576': No such file
block_dim = 1024
size = 1048576
test reduction >>>>>>>>>>>>>>>>>>>>>>
***
*** Starting ...
***
***
*** h_dataIn 88 Results:1
*** gridSize Results:1024
*** sizeof(*h_dataOut) Results:4
*** cpu Bandwidth Results:4.38315 GB/s
*** cou Results:1.04858e+06
***
*** Results:
***    h_dataOut: 1.04858e+06
***    Num Elements: 1048576
***    Time to Copy to Device: 0.430577 ms
***    Copy Bandwidth: 9.74112 GB/s
***    Time to Copy from Device: 0.0128 ms
***    Copy Bandwidth: 0.0003125 GB/s
***    Time for Reduction: 0.259524 ms
***    Reduction Bandwidth: 16.1615 GB/s
***
1: 1kB >>>>>>>>>>>>>>>>>>>>>>
***
*** Starting ...
***
***
*** h_dataIn 88 Results:1
*** gridSize Results:1
*** sizeof(*h_dataOut) Results:4
*** cpu Bandwidth Results:4.26667 GB/s
*** cou Results:1024
***
*** Results:
***    h_dataOut: 1024
***    Num Elements: 1024
***    Time to Copy to Device: 0.023121 ms
***    Copy Bandwidth: 0.177155 GB/s
***    Time to Copy from Device: 0.01313 ms
***    Copy Bandwidth: 0.000304646 GB/s
***    Time for Reduction: 0.038521 ms
***    Reduction Bandwidth: 0.106332 GB/s
***
2: 2kB >>>>>>>>>>>>>>>>>>>>>>
***
*** Starting ...
***
***
*** h_dataIn 88 Results:1
*** gridSize Results:2
*** sizeof(*h_dataOut) Results:4
*** cpu Bandwidth Results:4.33439 GB/s
*** cou Results:2048
***
*** Results:
***    h_dataOut: 2048
***    Num Elements: 2048
***    Time to Copy to Device: 0.02326 ms
***    Copy Bandwidth: 0.352193 GB/s
***    Time to Copy from Device: 0.01302 ms
***    Copy Bandwidth: 0.00030722 GB/s
***    Time for Reduction: 0.043411 ms
***    Reduction Bandwidth: 0.188708 GB/s
***
3: 4kB >>>>>>>>>>>>>>>>>>>>>>
***
*** Starting ...
***
***
*** h_dataIn 88 Results:1
*** gridSize Results:4
*** sizeof(*h_dataOut) Results:4
*** cpu Bandwidth Results:4.36907 GB/s
*** cou Results:4096
***
*** Results:
***    h_dataOut: 4096
***    Num Elements: 4096
***    Time to Copy to Device: 0.024301 ms
***    Copy Bandwidth: 0.674211 GB/s
***    Time to Copy from Device: 0.01282 ms
***    Copy Bandwidth: 0.000312012 GB/s
***    Time for Reduction: 0.038841 ms
***    Reduction Bandwidth: 0.421822 GB/s
***
4: 8kB >>>>>>>>>>>>>>>>>>>>>>
***
*** Starting ...
***
***
*** h_dataIn 88 Results:1
*** gridSize Results:8
*** sizeof(*h_dataOut) Results:4
*** cpu Bandwidth Results:4.38075 GB/s
*** cou Results:8192
***
*** Results:
***    h_dataOut: 8192
***    Num Elements: 8192
***    Time to Copy to Device: 0.032371 ms
***    Copy Bandwidth: 1.01226 GB/s
***    Time to Copy from Device: 0.0133 ms
***    Copy Bandwidth: 0.000300752 GB/s
***    Time for Reduction: 0.039911 ms
***    Reduction Bandwidth: 0.821027 GB/s
***
5: 16kB >>>>>>>>>>>>>>>>>>>>>>
***
*** Starting ...
***
***
*** h_dataIn 88 Results:1
*** gridSize Results:16
*** sizeof(*h_dataOut) Results:4
*** cpu Bandwidth Results:4.38955 GB/s
*** cou Results:16384
***
*** Results:
***    h_dataOut: 16384
***    Num Elements: 16384
***    Time to Copy to Device: 0.033261 ms
***    Copy Bandwidth: 1.97036 GB/s
***    Time to Copy from Device: 0.012881 ms
***    Copy Bandwidth: 0.000310535 GB/s
***    Time for Reduction: 0.04115 ms
***    Reduction Bandwidth: 1.59261 GB/s
***
6: 24kB >>>>>>>>>>>>>>>>>>>>>>
***
*** Starting ...
***
***
*** h_dataIn 88 Results:1
*** gridSize Results:20
*** sizeof(*h_dataOut) Results:4
*** cpu Bandwidth Results:4.39249 GB/s
*** cou Results:20480
***
*** Results:
***    h_dataOut: 20480
***    Num Elements: 20480
***    Time to Copy to Device: 0.036831 ms
***    Copy Bandwidth: 2.22421 GB/s
***    Time to Copy from Device: 0.012701 ms
***    Copy Bandwidth: 0.000314936 GB/s
***    Time for Reduction: 0.04153 ms
***    Reduction Bandwidth: 1.97255 GB/s
***
7: 32kB >>>>>>>>>>>>>>>>>>>>>>
***
*** Starting ...
***
***
*** h_dataIn 88 Results:1
*** gridSize Results:32
*** sizeof(*h_dataOut) Results:4
*** cpu Bandwidth Results:4.39397 GB/s
*** cou Results:32768
***
*** Results:
***    h_dataOut: 32768
***    Num Elements: 32768
***    Time to Copy to Device: 0.044161 ms
***    Copy Bandwidth: 2.96805 GB/s
***    Time to Copy from Device: 0.01293 ms
***    Copy Bandwidth: 0.000309358 GB/s
***    Time for Reduction: 0.044161 ms
***    Reduction Bandwidth: 2.96805 GB/s
***
8: 36kB >>>>>>>>>>>>>>>>>>>>>>
***
*** Starting ...
***
***
*** h_dataIn 88 Results:1
*** gridSize Results:36
*** sizeof(*h_dataOut) Results:4
*** cpu Bandwidth Results:4.39498 GB/s
*** cou Results:36864
***
*** Results:
***    h_dataOut: 36864
***    Num Elements: 36864
***    Time to Copy to Device: 0.04621 ms
***    Copy Bandwidth: 3.191 GB/s
***    Time to Copy from Device: 0.0127 ms
***    Copy Bandwidth: 0.000314961 GB/s
***    Time for Reduction: 0.04504 ms
***    Reduction Bandwidth: 3.27389 GB/s
***
9: 40kB >>>>>>>>>>>>>>>>>>>>>>
***
*** Starting ...
***
***
*** h_dataIn 88 Results:1
*** gridSize Results:40
*** sizeof(*h_dataOut) Results:4
*** cpu Bandwidth Results:4.39473 GB/s
*** cou Results:40960
***
*** Results:
***    h_dataOut: 40960
***    Num Elements: 40960
***    Time to Copy to Device: 0.048471 ms
***    Copy Bandwidth: 3.38017 GB/s
***    Time to Copy from Device: 0.01259 ms
***    Copy Bandwidth: 0.000317712 GB/s
***    Time for Reduction: 0.045161 ms
***    Reduction Bandwidth: 3.62791 GB/s
***
10: 48kB >>>>>>>>>>>>>>>>>>>>>>
***
*** Starting ...
***
***
*** h_dataIn 88 Results:1
*** gridSize Results:48
*** sizeof(*h_dataOut) Results:4
*** cpu Bandwidth Results:4.39642 GB/s
*** cou Results:49152
***
*** Results:
***    h_dataOut: 49152
***    Num Elements: 49152
***    Time to Copy to Device: 0.05395 ms
***    Copy Bandwidth: 3.64426 GB/s
***    Time to Copy from Device: 0.012821 ms
***    Copy Bandwidth: 0.000311988 GB/s
***    Time for Reduction: 0.0473 ms
***    Reduction Bandwidth: 4.15662 GB/s
***
end ......
