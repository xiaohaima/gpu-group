import matplotlib.pyplot as plt
import numpy as np


data_am = np.array(["1", "2", "4", "8", "16", "24", "32", "36", "40", "48", "1024"])

#line_b32 = np.array([0.0916106, 0.135672, 0.256919, 0.388702, 0.51004, 0.540148, 0.598549, 0.610548, 0.619963, 0.635418, 0.719926])
line_b1024 = np.array([0.106332, 0.188708, 0.421822, 0.821027, 1.59261, 1.97255,
                       2.96805, 3.27389, 3.62791, 4.15662, 16.1615])
#line_b256 = np.array([0.104942, 0.213495, 0.410318, 0.648743, 1.33502, 1.55502, 2.11984, 2.32174, 2.4567, 2.74972, 5.27345])

# line = np.log(line)


plt.figure(figsize=(12, 7))

# plt.plot(data_am, line_b32, ":", label="block_dim = 32")
# plt.plot(data_am, line_b256, "--", label="block_dim = 256")
plt.plot(data_am, line_b1024, "-.", label="block_dim = 1024")
plt.xlabel("array size, unit: kB")
plt.ylabel("bandwidth, unit: GB/s")

plt.title("reduction: GPU parallel reduction3 version, vary the array size, unit: GB/s")
plt.legend()
plt.show()
