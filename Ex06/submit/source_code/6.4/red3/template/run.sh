#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o ex6.4_test.txt

echo "start ......"


block_dim=$1
size=$2

size1=1024 # 1kB
size2=$((2*1024)) # 2kB
size3=$((4*1024)) # 4kB
size4=$((8*1024)) # 8kB
size5=$((16*1024)) # 16kB
size6=$((20*1024)) # 24kB
size7=$((32*1024)) # 32kB
size8=$((36*1024)) # 36kB
size9=$((40*1024)) # 40kB
size10=$((48*1024)) # 48kB

size = $((1024*1024)) # 1MB

echo "block_dim = $block_dim" 
echo "size = $size" 

echo "test reduction >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -t $block_dim -s $size

echo "1: 1kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -t $block_dim -s $size1

echo "2: 2kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -t $block_dim -s $size2

echo "3: 4kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -t $block_dim -s $size3

echo "4: 8kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -t $block_dim -s $size4

echo "5: 16kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -t $block_dim -s $size5

echo "6: 24kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -t $block_dim -s $size6

echo "7: 32kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -t $block_dim -s $size7

echo "8: 36kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -t $block_dim -s $size8

echo "9: 40kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -t $block_dim -s $size9

echo "10: 48kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/reduction -t $block_dim -s $size10



echo "end ......"






