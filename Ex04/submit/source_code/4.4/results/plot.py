#! /usr/bin/env python

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

matrix = 'matrix.out.txt'
matrix_throughput = "matrix_throughput.out_result.txt"
matrix_throughput_control = "matrix_throughput.control.txt"

def plot_matrix_by_size():
    matrix_csv = None
    with open(matrix, "r") as f:
        matrix_csv = pd.read_csv(f)
    matrix_csv.columns = ["problemsize", "time in µs", "time in s", "Total FLOP per Matrix", "FLOP/s"]
    matrix_csv["time in s"] = matrix_csv["time in s"].replace({" s":""}, regex=True).astype(float)
    print(matrix_csv["time in s"])
    plt.plot(matrix_csv["problemsize"], matrix_csv["time in s"])
    plt.xlabel("Matrix X-Dimension")
    plt.ylabel("Time in s")
    plt.xticks(np.arange(256,3072,256))
    plt.yticks(np.arange(0.,30,2.5))
    plt.savefig("matrix_problemsize.png")

def plot_problemsize_gflops():
    matrix_csv = None
    with open(matrix, "r") as f:
        matrix_csv = pd.read_csv(f)
    matrix_csv.columns = ["problemsize", "time in µs", "time in s", "Total FLOP per Matrix", "FLOP/s"]
    matrix_csv["FLOP/s"] = matrix_csv["FLOP/s"].replace({" flops":""}, regex=True).astype(float)
    plt.plot(matrix_csv["problemsize"],matrix_csv["FLOP/s"].divide(1e9))
    plt.xticks(np.arange(256,3072,256))
    plt.xlabel("Matrix X-Dimension")
    plt.ylabel("GFLOP/s")
    plt.savefig("matrix_problemsize_gflops")

def plot_sustained_flops():
    matrix_csv = None
    control_csv = None
    with open(matrix_throughput, "r") as f:
        matrix_csv = pd.read_csv(f)
    with open(matrix_throughput_control, "r") as f:
        control_csv = pd.read_csv(f)
    matrix_csv.columns = ["problemsize", "time in µs", "time in s", "Total FLOP per Matrix", "FLOP/s"]
    control_csv.columns = ["problemsize", "time in µs", "time in s", "Total FLOP per Matrix", "FLOP/s"]
    matrix_csv["FLOP/s"] = matrix_csv["FLOP/s"].replace({" flops":""}, regex=True).astype(float)
    control_csv["FLOP/s"] = control_csv["FLOP/s"].replace({" flops":""}, regex=True).astype(float)

    plt.plot(np.arange(len(matrix_csv["FLOP/s"])),matrix_csv["FLOP/s"].divide(1e9))
    plt.plot(np.arange(len(control_csv["FLOP/s"])), control_csv["FLOP/s"].divide(1e9))
    plt.plot(np.arange(len(matrix_csv["FLOP/s"])), [np.mean((matrix_csv["FLOP/s"]))/1e9]*len(matrix_csv["FLOP/s"]), label="Mean N=1792")
    plt.plot(np.arange(len(control_csv["FLOP/s"])), [np.mean((control_csv["FLOP/s"]))/1e9]* len(control_csv["FLOP/s"]), label="Mean N=512")
    plt.xlabel("Number of runs")
    plt.ylabel("GFLOP/s")
    plt.legend()
    plt.savefig("matrix_sustained_flops")
    print(np.mean(matrix_csv["FLOP/s"])/1e9)
    print(np.mean(control_csv["FLOP/s"])/1e9)


plot_matrix_by_size()
plt.clf()
plot_problemsize_gflops()
plt.clf()
plot_sustained_flops()