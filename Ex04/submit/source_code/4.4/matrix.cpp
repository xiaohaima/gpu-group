#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
#include <stdio.h>
#include <random>
#include <ctime>
#include "chTimer.h"

class Matrix {
    private:
        std::vector<double> _data;
        uint _x_dim, _y_dim;
    public:
        Matrix(uint x_dim, uint y_dim) {
            _x_dim = x_dim;
            _y_dim = y_dim;
            _data = std::vector<double>(_x_dim *_y_dim, 0.0);
        }

        double operator()(uint x, uint y) const{
            return _data[y*_x_dim + x];
        }

        double & operator()(uint x, uint y) {
            return _data[y*_x_dim+x];
        }

        uint get_x_dim() {
            return _x_dim;
        }

        uint get_y_dim() {
            return _y_dim;
        }

        std::string to_string() {
            std::stringstream sstream;
            sstream << "[ ";
            for(uint y = 0; y < _y_dim; y++) {
                if(y != 0) {
                    sstream << "  [ ";
                }
                else {
                    sstream << "[ ";
                }
                for(uint x = 0; x < _x_dim; x++) {
                    sstream << _data[y*_x_dim + x];
                    if (x != _x_dim - 1) {
                        sstream << "\t";
                    }
                }
                if(y != _y_dim - 1) {
                    sstream << " ]" << std::endl;
                }
            }
            sstream << " ] ]";
            return sstream.str();
        }

        Matrix operator*(Matrix &B) {
            uint b_x_dim = B.get_x_dim();
            if(_y_dim != b_x_dim) {
                //error case
            }
            Matrix C = Matrix(b_x_dim,_y_dim);
            for(uint i = 0; i < _y_dim; i++) {
                for(uint j = 0; j < b_x_dim; j++) {
                    for(uint k = 0; k < _y_dim; k++) {
                        uint index = k*get_x_dim() + i;
                        C(i,j) += (_data[index] * B(k,j));
                    }
                }
            }
            return C;
        }

    friend std::ostream& operator<<(std::ostream &os, Matrix &m);

};
std::ostream& operator<<(std::ostream& os, Matrix &m) {
    os << m.to_string();
    return os;
}

Matrix init_matrix(uint size, bool mul) {
    Matrix A = Matrix(size, size);
    for(uint x = 0; x < size; x++) {
        for(uint y = 0; y < size; y++) {
            if (!mul) {
                A(x,y) = x+y;
            } else {
                A(x,y) = x*y;
            }
        }
    }
    return A;
}

void benchmark_mul(Matrix &A, Matrix &B, Matrix *C, uint64_t *flop_counter) {
    uint a_y_dim = A.get_y_dim();
    uint b_x_dim = B.get_x_dim();
    if(a_y_dim != b_x_dim) {
        //error case
    }
    *flop_counter = 0;
    for(uint i = 0; i < a_y_dim; i++) {
        for(uint j = 0; j < b_x_dim; j++) {
            for(uint k = 0; k < a_y_dim; k++) {
                (*C)(i,j) += A(k,i) * B(k,j);
                *flop_counter += 2;
            }
        }
    }
}


void matrix_mul_test() {
    Matrix A = Matrix(5,5);
    Matrix B = Matrix(5,5);
    for(int i = 0; i < 5;i++) {
        for(int j = 0; j < 5;j++) {
            A(i,j) = i + j;
            B(i,j) = i * j;
        }
    }

    auto C = A*B;
    std::cout <<  C << std::endl;
    auto D = Matrix(5,5);
    uint64_t fp;
    benchmark_mul(A,B, &D, &fp);
    std::cout << C << std::endl;
}

void benchmark_matrix_mul(uint problemsize, uint ITERATIONS=10) {
    uint64_t total_flop_counter = 0;
    double total_time = 0.0;
    for(uint i = 0; i < ITERATIONS; i++) {
        uint64_t flop_counter = 0;
        auto A = init_matrix(problemsize, false);
        auto B = init_matrix(problemsize, true);
        auto C = Matrix(problemsize, problemsize);
        chTimerTimestamp start, stop;
        chTimerGetTime(&start);
        benchmark_mul(A, B, &C, &flop_counter);
        chTimerGetTime(&stop);
        total_flop_counter += flop_counter;
        total_time += (1e6 * chTimerElapsedTime(&start, &stop));
    }
    double avg_time_us = (total_time/ITERATIONS);
    double avg_time_s = (total_time/ITERATIONS/1e6);
    uint64_t avg_flop = total_flop_counter/ITERATIONS;
    uint64_t avg_flops = avg_flop/avg_time_s;
    std::cout << problemsize << ","<< avg_time_us << " us," << avg_time_s << " s," << avg_flop <<" flop," << avg_flops << " flops" << std::endl;   
}

int main(int argc, char ** argv) {
    #ifdef VERIFY
    matrix_mul_test();
    #endif
    #ifndef VERIFY
    auto iterations = 10;
    if(argc < 2) {
        std::cerr << "Expected problem size as argument." << std::endl;
        return 1;
    } else if (argc == 3){
        iterations = atoi(argv[2]);
    }
    auto problem_size = atoi(argv[1]);
    benchmark_matrix_mul(problem_size, iterations);
    #endif
    return 0;
}
