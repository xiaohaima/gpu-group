#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o results/matrix_throughput.out_result.txt


MIN=0
MAX=100
SIZE=1792

for ((i=MIN;i<=MAX;i+=1)); do
    ./bin/matrix $SIZE 3
done
