#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o results/matrix.out.txt


MIN=128
MAX=3072
INCREMENT=128

for ((i=MIN;i<=MAX;i+=INCREMENT)); do
    ./bin/matrix $i
done