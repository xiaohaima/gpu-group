#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o fixsize_shared_to_global.csv

echo "start ......"



# thread block test
grid_dim1=1
grid_dim2=2
grid_dim3=4
grid_dim4=8
grid_dim5=16
grid_dim6=32
itr=100 # 100 iterations
# block dim test
block_dim1=16
block_dim2=32
block_dim3=64
block_dim4=128
block_dim5=256
block_dim6=512



size=$((32*1024)) # 32kB



# 1: block_dim1=16
echo "1: block_dim1=16 >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --shared2global -s $size -t $block_dim1 -g $grid_dim1 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim1 -g $grid_dim2 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim1 -g $grid_dim3 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim1 -g $grid_dim4 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim1 -g $grid_dim5 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim1 -g $grid_dim6 -i $itr

# 2: block_dim2=32
echo "2: block_dim2=32 >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --shared2global -s $size -t $block_dim2 -g $grid_dim1 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim2 -g $grid_dim2 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim2 -g $grid_dim3 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim2 -g $grid_dim4 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim2 -g $grid_dim5 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim2 -g $grid_dim6 -i $itr

# 3: block_dim3=64
echo "3: block_dim3=64 >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --shared2global -s $size -t $block_dim3 -g $grid_dim1 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim3 -g $grid_dim2 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim3 -g $grid_dim3 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim3 -g $grid_dim4 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim3 -g $grid_dim5 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim3 -g $grid_dim6 -i $itr

# 4: block_dim4=128
echo "4: block_dim4=128 >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --shared2global -s $size -t $block_dim4 -g $grid_dim1 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim4 -g $grid_dim2 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim4 -g $grid_dim3 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim4 -g $grid_dim4 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim4 -g $grid_dim5 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim4 -g $grid_dim6 -i $itr

# 5: block_dim5=256
echo "5: block_dim5=256 >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --shared2global -s $size -t $block_dim5 -g $grid_dim1 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim5 -g $grid_dim2 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim5 -g $grid_dim3 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim5 -g $grid_dim4 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim5 -g $grid_dim5 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim5 -g $grid_dim6 -i $itr

# 6: block_dim6=512
echo "6: block_dim6=512 >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --shared2global -s $size -t $block_dim6 -g $grid_dim1 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim6 -g $grid_dim2 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim6 -g $grid_dim3 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim6 -g $grid_dim4 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim6 -g $grid_dim5 -i $itr
./bin/memCpy --shared2global -s $size -t $block_dim6 -g $grid_dim6 -i $itr




echo "end ......"