/******************************************************************************
 *
 *Computer Engineering Group, Heidelberg University - GPU Computing Exercise 04
 *
 *                  Group : 09
 *
 *                   File : kernel.cu
 *
 *                Purpose : Memory Operations Benchmark
 *
 ******************************************************************************/


//
// Test Kernel
//

__global__ void 
globalMem2SharedMem
//(/*TODO Parameters*/)
(int shmSize, float *global_data)
{
	/*TODO Kernel Code*/
	int size = shmSize / sizeof(float);

	extern __shared__ float shared_data[];
	// float *shared_data = &shared[0];

	for(int i = threadIdx.x; i < size; i += blockDim.x) {
        shared_data[i] = global_data[i];
    }
	__syncthreads();

}

void globalMem2SharedMem_Wrapper(dim3 gridSize, dim3 blockSize, int shmSize, float *global_data /* TODO Parameters*/) {
	globalMem2SharedMem<<< gridSize, blockSize, shmSize >>>( shmSize, global_data /* TODO Parameters */);
}

__global__ void 
SharedMem2globalMem
//(/*TODO Parameters*/)
(int size, float *global_data)
{
	/*TODO Kernel Code*/
	extern __shared__ float shared_data[];
	// float *shared_data = &shared[0];

	for(int i = threadIdx.x; i < size; i += blockDim.x) {
		global_data[i] = shared_data[i];
    }
	__syncthreads();

}

void SharedMem2globalMem_Wrapper(dim3 gridSize, dim3 blockSize, int shmSize, int size, float *global_data /* TODO Parameters*/) {
	SharedMem2globalMem<<< gridSize, blockSize, shmSize >>>( size, global_data /* TODO Parameters */);
}

__global__ void 
SharedMem2Registers
//(/*TODO Parameters*/)
(int size, float *outFloat)
{
	/*TODO Kernel Code*/
	extern __shared__ float shared_data[];
	// float *shared_data = &shared[0];

	// float var;

	for(int i = threadIdx.x; i < size; i += blockDim.x) {
		float var = shared_data[i];
		*outFloat = var;
    }
	__syncthreads();
	
}

void SharedMem2Registers_Wrapper(dim3 gridSize, dim3 blockSize, int shmSize, int size, float *outFloat /* TODO Parameters*/) {
	SharedMem2Registers<<< gridSize, blockSize, shmSize >>>( size, outFloat /* TODO Parameters */);
}

__global__ void 
Registers2SharedMem
//(/*TODO Parameters*/)
(int size, float *outFloat)
{
	/*TODO Kernel Code*/
	extern __shared__ float shared_data[];
	// float *shared_data = &shared[0];

	// float var;

	for(int i = threadIdx.x; i < size; i += blockDim.x) {
		float var = 1.0;
		shared_data[i] = var;
		*outFloat = var;
    }
	__syncthreads();

}

void Registers2SharedMem_Wrapper(dim3 gridSize, dim3 blockSize, int shmSize, int size, float *outFloat /* TODO Parameters*/) {
	Registers2SharedMem<<< gridSize, blockSize, shmSize >>>( size, outFloat /* TODO Parameters */);
}

__global__ void 
bankConflictsRead
//(/*TODO Parameters*/)
( )
{
	/*TODO Kernel Code*/
}

void bankConflictsRead_Wrapper(dim3 gridSize, dim3 blockSize, int shmSize /* TODO Parameters*/) {
	bankConflictsRead<<< gridSize, blockSize, shmSize >>>( /* TODO Parameters */);
}
