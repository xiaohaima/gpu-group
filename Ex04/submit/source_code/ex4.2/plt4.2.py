import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

########################################################################################################
data = pd.read_csv("global_to_shared.csv", sep=",", decimal=".")

data_am = ["1kB", "2kB", "4kB", "8kB", "16kB", "24kB", "32kB", "36kB", "40kB", "48kB"]

print(data.head())
print(data.columns)
line1 = data["16"].to_numpy()
line2 = data["32"].to_numpy()
line3 = data["64"].to_numpy()
line4 = data["128"].to_numpy()
line5 = data["256"].to_numpy()
line6 = data["512"].to_numpy()
linestyles = [':', ':', ':', ':', '--', '-.', 'None']
colors = ['brown', 'r', 'g', 'b', 'y', 'magenta', 'None']

plt.figure(figsize=(12, 7))
x = range(len(data_am))
plt.xticks(x, data_am, rotation=45)
plt.plot(line1, linestyle=linestyles[0], c=colors[0], label="block_dim = 16")
plt.plot(line2, linestyle=linestyles[1], c=colors[1], label="block_dim = 32")
plt.plot(line3, linestyle=linestyles[2], c=colors[2], label="block_dim = 64")
plt.plot(line4, linestyle=linestyles[3], c=colors[3], label="block_dim = 128")
plt.plot(line5, linestyle=linestyles[4], c=colors[4], label="block_dim = 256")
plt.plot(line6, linestyle=linestyles[5], c=colors[5], label="block_dim = 512")

plt.title("Copy from global memory to shared memory, from 1KB-48kB, unit: GB/s")
plt.legend()
plt.show()



########################################################################################################
data = pd.read_csv("shared_to_global.csv", sep=",", decimal=".")

data_am = ["1kB", "2kB", "4kB", "8kB", "16kB", "24kB", "32kB", "36kB", "40kB", "48kB"]

print(data.head())
print(data.columns)
line1 = data["16"].to_numpy()
line2 = data["32"].to_numpy()
line3 = data["64"].to_numpy()
line4 = data["128"].to_numpy()
line5 = data["256"].to_numpy()
line6 = data["512"].to_numpy()
linestyles = [':', ':', ':', ':', '--', '-.', 'None']
colors = ['brown', 'r', 'g', 'b', 'y', 'magenta', 'None']

plt.figure(figsize=(12, 7))
x = range(len(data_am))
plt.xticks(x, data_am, rotation=45)
plt.plot(line1, linestyle=linestyles[0], c=colors[0], label="block_dim = 16")
plt.plot(line2, linestyle=linestyles[1], c=colors[1], label="block_dim = 32")
plt.plot(line3, linestyle=linestyles[2], c=colors[2], label="block_dim = 64")
plt.plot(line4, linestyle=linestyles[3], c=colors[3], label="block_dim = 128")
plt.plot(line5, linestyle=linestyles[4], c=colors[4], label="block_dim = 256")
plt.plot(line6, linestyle=linestyles[5], c=colors[5], label="block_dim = 512")

plt.title("Copy from shared memory to global memory, from 1KB-48kB, unit: GB/s")
plt.legend()
plt.show()



########################################################################################################
data = pd.read_csv("fixsize_global_to_shared.csv", sep=",", decimal=".")

data_am = ["16threads", "32threads", "64threads", "128threads", "256threads", "512threads"]

print(data.head())
print(data.columns)
line1 = data["1"].to_numpy() / 1
line2 = data["2"].to_numpy() / 2
line3 = data["4"].to_numpy() / 4
line4 = data["8"].to_numpy() / 8
line5 = data["16"].to_numpy() / 16
line6 = data["32"].to_numpy() / 32
linestyles = [':', ':', ':', ':', '--', '-.', 'None']
colors = ['brown', 'r', 'g', 'b', 'y', 'magenta', 'None']

plt.figure(figsize=(12, 7))
x = range(len(data_am))
plt.xticks(x, data_am, rotation=45)
plt.plot(line1, linestyle=linestyles[0], c=colors[0], label="grid_dim = 1")
plt.plot(line2, linestyle=linestyles[1], c=colors[1], label="grid_dim = 2")
plt.plot(line3, linestyle=linestyles[2], c=colors[2], label="grid_dim = 4")
plt.plot(line4, linestyle=linestyles[3], c=colors[3], label="grid_dim = 8")
plt.plot(line5, linestyle=linestyles[4], c=colors[4], label="grid_dim = 16")
plt.plot(line6, linestyle=linestyles[5], c=colors[5], label="grid_dim = 32")

plt.title("Copy from global memory to shared memory, size 32kB, unit: GB/s")
plt.legend()
plt.show()



########################################################################################################
data = pd.read_csv("fixsize_shared_to_global.csv", sep=",", decimal=".")

data_am = ["16threads", "32threads", "64threads", "128threads", "256threads", "512threads"]

print(data.head())
print(data.columns)
line1 = data["1"].to_numpy() / 1
line2 = data["2"].to_numpy() / 2
line3 = data["4"].to_numpy() / 4
line4 = data["8"].to_numpy() / 8
line5 = data["16"].to_numpy() / 16
line6 = data["32"].to_numpy() / 32
linestyles = [':', ':', ':', ':', '--', '-.', 'None']
colors = ['brown', 'r', 'g', 'b', 'y', 'magenta', 'None']

plt.figure(figsize=(12, 7))
x = range(len(data_am))
plt.xticks(x, data_am, rotation=45)
plt.plot(line1, linestyle=linestyles[0], c=colors[0], label="grid_dim = 1")
plt.plot(line2, linestyle=linestyles[1], c=colors[1], label="grid_dim = 2")
plt.plot(line3, linestyle=linestyles[2], c=colors[2], label="grid_dim = 4")
plt.plot(line4, linestyle=linestyles[3], c=colors[3], label="grid_dim = 8")
plt.plot(line5, linestyle=linestyles[4], c=colors[4], label="grid_dim = 16")
plt.plot(line6, linestyle=linestyles[5], c=colors[5], label="grid_dim = 32")

plt.title("Copy from shared memory to global memory, size 32kB, unit: GB/s")
plt.legend()
plt.show()




########################################################################################################
data = pd.read_csv("shared_to_register.csv", sep=",", decimal=".")

data_am = ["1kB", "2kB", "4kB", "8kB", "16kB", "24kB", "32kB", "36kB", "40kB", "48kB"]

print(data.head())
print(data.columns)
line1 = data["16"].to_numpy()
line2 = data["32"].to_numpy()
line3 = data["64"].to_numpy()
line4 = data["128"].to_numpy()
line5 = data["256"].to_numpy()
line6 = data["512"].to_numpy()
linestyles = [':', ':', ':', ':', '--', '-.', 'None']
colors = ['brown', 'r', 'g', 'b', 'y', 'magenta', 'None']

plt.figure(figsize=(12, 7))
x = range(len(data_am))
plt.xticks(x, data_am, rotation=45)
plt.plot(line1, linestyle=linestyles[0], c=colors[0], label="block_dim = 16")
plt.plot(line2, linestyle=linestyles[1], c=colors[1], label="block_dim = 32")
plt.plot(line3, linestyle=linestyles[2], c=colors[2], label="block_dim = 64")
plt.plot(line4, linestyle=linestyles[3], c=colors[3], label="block_dim = 128")
plt.plot(line5, linestyle=linestyles[4], c=colors[4], label="block_dim = 256")
plt.plot(line6, linestyle=linestyles[5], c=colors[5], label="block_dim = 512")

plt.title("Copy from shared memory to register, from 1KB-48kB, unit: GB/s")
plt.legend()
plt.show()




########################################################################################################
data = pd.read_csv("register_to_shared.csv", sep=",", decimal=".")

data_am = ["1kB", "2kB", "4kB", "8kB", "16kB", "24kB", "32kB", "36kB", "40kB", "48kB"]

print(data.head())
print(data.columns)
line1 = data["16"].to_numpy()
line2 = data["32"].to_numpy()
line3 = data["64"].to_numpy()
line4 = data["128"].to_numpy()
line5 = data["256"].to_numpy()
line6 = data["512"].to_numpy()
linestyles = [':', ':', ':', ':', '--', '-.', 'None']
colors = ['brown', 'r', 'g', 'b', 'y', 'magenta', 'None']

plt.figure(figsize=(12, 7))
x = range(len(data_am))
plt.xticks(x, data_am, rotation=45)
plt.plot(line1, linestyle=linestyles[0], c=colors[0], label="block_dim = 16")
plt.plot(line2, linestyle=linestyles[1], c=colors[1], label="block_dim = 32")
plt.plot(line3, linestyle=linestyles[2], c=colors[2], label="block_dim = 64")
plt.plot(line4, linestyle=linestyles[3], c=colors[3], label="block_dim = 128")
plt.plot(line5, linestyle=linestyles[4], c=colors[4], label="block_dim = 256")
plt.plot(line6, linestyle=linestyles[5], c=colors[5], label="block_dim = 512")

plt.title("Copy from register to shared memory, from 1KB-48kB, unit: GB/s")
plt.legend()
plt.show()