#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o register_to_shared.csv

echo "start ......"


grid_dim=1 # one thread block
itr=100 # 100 iterations
# block dim test
block_dim1=16
block_dim2=32
block_dim3=64
block_dim4=128
block_dim5=256
block_dim6=512



size1=1024 # 1kB
size2=$((2*1024)) # 2kB
size3=$((4*1024)) # 4kB
size4=$((8*1024)) # 8kB
size5=$((16*1024)) # 16kB
size6=$((20*1024)) # 24kB
size7=$((32*1024)) # 32kB
size8=$((36*1024)) # 36kB
size9=$((40*1024)) # 40kB
size10=$((48*1024)) # 48kB





# 1: 1kB
echo "1: 1kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --register2shared -s $size1 -t $block_dim1 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size1 -t $block_dim2 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size1 -t $block_dim3 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size1 -t $block_dim4 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size1 -t $block_dim5 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size1 -t $block_dim6 -g $grid_dim -i $itr

# 2: 2kB
echo "2: 2kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --register2shared -s $size2 -t $block_dim1 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size2 -t $block_dim2 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size2 -t $block_dim3 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size2 -t $block_dim4 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size2 -t $block_dim5 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size2 -t $block_dim6 -g $grid_dim -i $itr

# 3: 4kB
echo "3: 4kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --register2shared -s $size3 -t $block_dim1 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size3 -t $block_dim2 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size3 -t $block_dim3 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size3 -t $block_dim4 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size3 -t $block_dim5 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size3 -t $block_dim6 -g $grid_dim -i $itr

# 4: 8kB
echo "4: 8kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --register2shared -s $size4 -t $block_dim1 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size4 -t $block_dim2 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size4 -t $block_dim3 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size4 -t $block_dim4 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size4 -t $block_dim5 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size4 -t $block_dim6 -g $grid_dim -i $itr

# 5: 16kB
echo "5: 16kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --register2shared -s $size5 -t $block_dim1 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size5 -t $block_dim2 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size5 -t $block_dim3 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size5 -t $block_dim4 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size5 -t $block_dim5 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size5 -t $block_dim6 -g $grid_dim -i $itr

# 6: 24kB
echo "6: 24kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --register2shared -s $size6 -t $block_dim1 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size6 -t $block_dim2 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size6 -t $block_dim3 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size6 -t $block_dim4 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size6 -t $block_dim5 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size6 -t $block_dim6 -g $grid_dim -i $itr

# 7: 32kB
echo "7: 32kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --register2shared -s $size7 -t $block_dim1 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size7 -t $block_dim2 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size7 -t $block_dim3 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size7 -t $block_dim4 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size7 -t $block_dim5 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size7 -t $block_dim6 -g $grid_dim -i $itr

# 8: 36kB
echo "8: 36kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --register2shared -s $size8 -t $block_dim1 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size8 -t $block_dim2 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size8 -t $block_dim3 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size8 -t $block_dim4 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size8 -t $block_dim5 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size8 -t $block_dim6 -g $grid_dim -i $itr

# 9: 40kB
echo "9: 40kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --register2shared -s $size9 -t $block_dim1 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size9 -t $block_dim2 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size9 -t $block_dim3 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size9 -t $block_dim4 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size9 -t $block_dim5 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size9 -t $block_dim6 -g $grid_dim -i $itr

# 10: 48kB
echo "10: 48kB >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --register2shared -s $size10 -t $block_dim1 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size10 -t $block_dim2 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size10 -t $block_dim3 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size10 -t $block_dim4 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size10 -t $block_dim5 -g $grid_dim -i $itr
./bin/memCpy --register2shared -s $size10 -t $block_dim6 -g $grid_dim -i $itr




echo "end ......"