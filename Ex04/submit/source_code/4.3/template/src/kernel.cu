/******************************************************************************
 *
 *Computer Engineering Group, Heidelberg University - GPU Computing Exercise 04
 *
 *                  Group : 09
 *
 *                   File : kernel.cu
 *
 *                Purpose : Memory Operations Benchmark
 *
 ******************************************************************************/


//
// Test Kernel
//

__global__ void 
globalMem2SharedMem
//(/*TODO Parameters*/)
( )
{
	/*TODO Kernel Code*/
}

void globalMem2SharedMem_Wrapper(dim3 gridSize, dim3 blockSize, int shmSize /* TODO Parameters*/) {
	globalMem2SharedMem<<< gridSize, blockSize, shmSize >>>( /* TODO Parameters */);
}

__global__ void 
SharedMem2globalMem
//(/*TODO Parameters*/)
( )
{
	/*TODO Kernel Code*/
}
void SharedMem2globalMem_Wrapper(dim3 gridSize, dim3 blockSize, int shmSize /* TODO Parameters*/) {
	SharedMem2globalMem<<< gridSize, blockSize, shmSize >>>( /* TODO Parameters */);
}

__global__ void 
SharedMem2Registers
//(/*TODO Parameters*/)
( )
{
	/*TODO Kernel Code*/
}
void SharedMem2Registers_Wrapper(dim3 gridSize, dim3 blockSize, int shmSize /* TODO Parameters*/) {
	SharedMem2Registers<<< gridSize, blockSize, shmSize >>>( /* TODO Parameters */);
}

__global__ void 
Registers2SharedMem
//(/*TODO Parameters*/)
( )
{
	/*TODO Kernel Code*/
}
void Registers2SharedMem_Wrapper(dim3 gridSize, dim3 blockSize, int shmSize /* TODO Parameters*/) {
	Registers2SharedMem<<< gridSize, blockSize, shmSize >>>( /* TODO Parameters */);
}
__global__ void 
bankConflictsRead
//(/*TODO Parameters*/)
(int mod, long *dClocks, int stride, int itrations )
{
	/*TODO Kernel Code*/
  clock_t t0 = clock64();
  clock_t t1 = t0;
  __shared__ float data[32*1024];
  int const offset = blockIdx.x * blockDim.x + threadIdx.x;
  //int i = -30;
  if (offset*stride < mod) {
    for (int k = 0; k < itrations; k ++ ) {
      float tmp = data[offset*stride];
      //i++;
    }
  }
  t1 = clock64();
  *dClocks = (t1 - t0);
  //*dClocks = i;
  //*dClocks = *dClocks+(t1 - t0);
}

void bankConflictsRead_Wrapper(dim3 gridSize, dim3 blockSize, int shmSize, long *dClocks, int stride, int itrations /* TODO Parameters*/) {
	bankConflictsRead<<< gridSize, blockSize, shmSize >>>( shmSize, dClocks, stride, itrations/* TODO Parameters */);
}
