#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o ex4.3_test.txt

echo "start ......"

grid_dim=$1

block_dim=$2

itr=$3

stri=$4

echo "grid_dim = $grid_dim" 
echo "block_dim = $block_dim" 
echo "itr = $itr" 
echo "stri = $stri" 

echo "test shared2register_conflict >>>>>>>>>>>>>>>>>>>>>>"
./bin/memCpy --shared2register_conflict -t $block_dim -g $grid_dim -i $itr -stride $stri





echo "end ......"

