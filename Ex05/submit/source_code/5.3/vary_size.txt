start ......
test matMul with different size using shared memory >>>>>>>>>>>>>>>>>>>>>>
 >>>>>>>>>> s 16
***
*** Starting ...
***
***    Matrix matrixWidth: 16
***
*** Grid Dim:  1x1x1
*** Block Dim: 16x16x1
***
 >>>>> shMatMul_Kernel ***
*** cpuTimer Results:
*** 0.00242 ms
***
*** Results:
***    Matrix Size: 256
***    Time to Copy to Device: 0.02361 ms
***    Copy Bandwidth: 0.0867429 GB/s
***    Time to Copy from Device: 0.014191 ms
***    Copy Bandwidth: 0.0721584 GB/s
***    Time for Matrix Multiplication: 0.03579 ms
***    Overall Time for Matrix Multiplication: 0.073591 ms
***
 >>>>>>>>>> s 32
***
*** Starting ...
***
***    Matrix matrixWidth: 32
***
*** Grid Dim:  2x2x1
*** Block Dim: 16x16x1
***
 >>>>> shMatMul_Kernel ***
*** cpuTimer Results:
*** 0.01989 ms
***
*** Results:
***    Matrix Size: 1024
***    Time to Copy to Device: 0.023141 ms
***    Copy Bandwidth: 0.354004 GB/s
***    Time to Copy from Device: 0.01564 ms
***    Copy Bandwidth: 0.261893 GB/s
***    Time for Matrix Multiplication: 0.039291 ms
***    Overall Time for Matrix Multiplication: 0.078072 ms
***
 >>>>>>>>>> s 64
***
*** Starting ...
***
***    Matrix matrixWidth: 64
***
*** Grid Dim:  4x4x1
*** Block Dim: 16x16x1
***
 >>>>> shMatMul_Kernel ***
*** cpuTimer Results:
*** 0.158423 ms
***
*** Results:
***    Matrix Size: 4096
***    Time to Copy to Device: 0.02594 ms
***    Copy Bandwidth: 1.26322 GB/s
***    Time to Copy from Device: 0.01612 ms
***    Copy Bandwidth: 1.01638 GB/s
***    Time for Matrix Multiplication: 0.039001 ms
***    Overall Time for Matrix Multiplication: 0.081061 ms
***
 >>>>>>>>>> s 256
***
*** Starting ...
***
***    Matrix matrixWidth: 256
***
*** Grid Dim:  16x16x1
*** Block Dim: 16x16x1
***
 >>>>> shMatMul_Kernel ***
*** cpuTimer Results:
*** 10.952 ms
***
*** Results:
***    Matrix Size: 65536
***    Time to Copy to Device: 0.077001 ms
***    Copy Bandwidth: 6.80885 GB/s
***    Time to Copy from Device: 0.148312 ms
***    Copy Bandwidth: 1.76752 GB/s
***    Time for Matrix Multiplication: 0.073751 ms
***    Overall Time for Matrix Multiplication: 0.299064 ms
***
 >>>>>>>>>> s 512
***
*** Starting ...
***
***    Matrix matrixWidth: 512
***
*** Grid Dim:  32x32x1
*** Block Dim: 16x16x1
***
 >>>>> shMatMul_Kernel ***
*** cpuTimer Results:
*** 222.533 ms
***
*** Results:
***    Matrix Size: 262144
***    Time to Copy to Device: 0.226603 ms
***    Copy Bandwidth: 9.25474 GB/s
***    Time to Copy from Device: 0.554609 ms
***    Copy Bandwidth: 1.89066 GB/s
***    Time for Matrix Multiplication: 0.324035 ms
***    Overall Time for Matrix Multiplication: 1.10525 ms
***
 >>>>>>>>>> s 1024
***
*** Starting ...
***
***    Matrix matrixWidth: 1024
***
*** Grid Dim:  64x64x1
*** Block Dim: 16x16x1
***
 >>>>> shMatMul_Kernel ***
*** cpuTimer Results:
*** 3391.6 ms
***
*** Results:
***    Matrix Size: 1048576
***    Time to Copy to Device: 0.742723 ms
***    Copy Bandwidth: 11.2944 GB/s
***    Time to Copy from Device: 1.91422 ms
***    Copy Bandwidth: 2.19113 GB/s
***    Time for Matrix Multiplication: 2.00787 ms
***    Overall Time for Matrix Multiplication: 4.66482 ms
***
 >>>>>>>>>> s 2048
***
*** Starting ...
***
***    Matrix matrixWidth: 2048
***
*** Grid Dim:  128x128x1
*** Block Dim: 16x16x1
***
 >>>>> shMatMul_Kernel ***
*** cpuTimer Results:
*** 27366 ms
***
*** Results:
***    Matrix Size: 4194304
***    Time to Copy to Device: 2.98082 ms
***    Copy Bandwidth: 11.2568 GB/s
***    Time to Copy from Device: 7.47238 ms
***    Copy Bandwidth: 2.24523 GB/s
***    Time for Matrix Multiplication: 15.5657 ms
***    Overall Time for Matrix Multiplication: 26.019 ms
***
end ......
