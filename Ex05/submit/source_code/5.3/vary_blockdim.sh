#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o vary_blockdim.txt

echo "start ......"

echo "test matMul with different blockdim using shared memory >>>>>>>>>>>>>>>>>>>>>>"


echo " >>>>>>>>>> block_dim = 1"
./bin/matMul -s 2048 -t 1

echo " >>>>>>>>>> block_dim = 2"
./bin/matMul -s 2048 -t 2
 
echo " >>>>>>>>>> block_dim = 4"
./bin/matMul -s 2048 -t 4
 
echo " >>>>>>>>>> block_dim = 8"
./bin/matMul -s 2048 -t 8
 
echo " >>>>>>>>>> block_dim = 16"
./bin/matMul -s 2048 -t 16

echo " >>>>>>>>>> block_dim = 32"
./bin/matMul -s 2048 -t 32



echo "end ......"