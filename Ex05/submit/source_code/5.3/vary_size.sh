#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o vary_size.txt

echo "start ......"

echo "test matMul with different size using shared memory >>>>>>>>>>>>>>>>>>>>>>"


echo " >>>>>>>>>> s 16"
./bin/matMul -s 16 -t 16

echo " >>>>>>>>>> s 32"
./bin/matMul -s 32 -t 16

echo " >>>>>>>>>> s 64"
./bin/matMul -s 64 -t 16

echo " >>>>>>>>>> s 256"
./bin/matMul -s 256 -t 16

echo " >>>>>>>>>> s 512"
./bin/matMul -s 512 -t 16

echo " >>>>>>>>>> s 1024"
./bin/matMul -s 1024 -t 16

echo " >>>>>>>>>> s 2048"
./bin/matMul -s 2048 -t 16



echo "end ......"