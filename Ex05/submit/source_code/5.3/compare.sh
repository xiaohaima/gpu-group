#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o compare.txt

echo "start ......"

echo "comparison with cpu and gpu naive version >>>>>>>>>>>>>>>>>>>>>>"

echo " >>>>>>>>>> cpu naive version -t 1"
./bin/matMul -shared -s 2048 -t 1

echo " >>>>>>>>>> cpu naive version -t 2"
./bin/matMul -shared -s 2048 -t 2

echo " >>>>>>>>>> cpu naive version -t 4"
./bin/matMul -shared -s 2048 -t 4

echo " >>>>>>>>>> cpu naive version -t 8"
./bin/matMul -shared -s 2048 -t 8

echo " >>>>>>>>>> cpu naive version -t 16"
./bin/matMul -shared -s 2048 -t 16

echo " >>>>>>>>>> cpu naive version -t 32"
./bin/matMul -shared -s 2048 -t 32





echo "end ......"
