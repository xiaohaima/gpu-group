import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

########################################################################################################
data_am = np.array(["1", "2", "4", "8", "16", "32"])

line = np.array([2657.51, 423.959, 88.3856, 32.6711, 26.0081, 25.3229])
line = np.log(line)


plt.figure(figsize=(12, 7))

plt.plot(data_am, line, ":")
plt.xlabel("blockdim")
plt.ylabel("log(runtime)")

plt.title("2048*2048 Matrix Multiply – GPU using shared memory, vary the threads-per-block, unit: ms")
plt.legend()
plt.show()


########################################################################################################
data_am = np.array(["16", "32", "64", "256", "512", "1024", "2048"])

copy_to_device = np.array([0.02361, 0.023141, 0.02594, 0.077001, 0.226603, 0.742723, 2.98082])
copy_to_device = np.log(copy_to_device)
copy_from_device = np.array([0.014191, 0.01564, 0.01612, 0.148312, 0.554609, 1.91422, 7.47238])
copy_from_device = np.log(copy_from_device)
matmul =  np.array([0.03579, 0.039291, 0.039001, 0.073751, 0.324035, 2.00787, 15.5657])
matmul = np.log(matmul)
overall = np.array([0.073591, 0.078072, 0.081061, 0.299064, 1.10525, 4.66482, 26.019])
overall = np.log(overall)

plt.figure(figsize=(12, 7))

plt.plot(data_am, copy_to_device, "r:", label="copy to device")
plt.plot(data_am, copy_from_device, "b:", label="copy from device")
plt.plot(data_am, matmul, "g:", label="matmul")
plt.plot(data_am, overall, "k:", label="overall")
plt.xlabel("matrix width")
plt.ylabel("log(runtime)")

plt.title("Matrix Multiply with blockdim 16 – GPU using shared memory, vary the matrix width, unit: ms")
plt.legend()
plt.show()


########################################################################################################
data_am = np.array(["1", "2", "4", "8", "16", "32"])

gpu_matmul = np.array([578.651, 214.805, 118.82, 43.6286, 25.2968, 28.6199])
gpu_matmul = np.log(gpu_matmul)
gpu_overall = np.array([588.84, 225.063, 129.069, 53.8941, 35.5776, 38.8736])
gpu_overall = np.log(gpu_overall)

shared_matmul = np.array([2647.06, 413.498, 77.9521, 22.2693, 15.5502, 14.9152])
shared_matmul = np.log(shared_matmul)
shared_overall = np.array([2657.51, 423.959, 88.3856, 32.6711, 26.0081, 25.3229])
shared_overall = np.log(shared_overall)

cpu = np.array([27110.8, 27200, 27175.9, 27164.2, 27155.3, 27023.5])
cpu = np.log(cpu)


plt.figure(figsize=(12, 7))

plt.plot(data_am, gpu_matmul, "r:", label="gpu naive matmul")
plt.plot(data_am, gpu_overall, "b:", label="gpu naive overall")
plt.plot(data_am, shared_matmul, "g:", label="gpu shared memory matmal")
plt.plot(data_am, shared_overall, "k:", label="gpu shared memory overall")
plt.plot(data_am, cpu, "y:", label="cpu")
plt.xlabel("blockdim")
plt.ylabel("log(runtime)")

plt.title("compare cpu, gpu naive and gpu using shared memory, vary the threads-per-block, unit: ms")
plt.legend()
plt.show()
