import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

data = pd.read_csv("ex5.2-opt.csv", sep=",", decimal=".")

data_am = ["1", "4", "8", "16", "20", "28", "32"]

print(data.head())
print(data.columns)
line = data["runtime"].to_numpy()
line = np.log(line)

linestyles = [':', ':', ':', ':', '--', '-.', '-', 'None']
colors = ['brown', 'r', 'g', 'b', 'y', 'magenta', 'c', 'None']

plt.figure(figsize=(12, 7))
x = range(len(data_am))
plt.xticks(x, data_am, rotation=0)
plt.plot(line, linestyle=linestyles[0], c=colors[0])


plt.title("2048*2048 Matrix Multiply – GPU naive version, vary the threads-per-block, unit: ms")
plt.legend()
plt.show()







