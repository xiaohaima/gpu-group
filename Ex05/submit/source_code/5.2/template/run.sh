#!/bin/bash
#SBATCH --gres=gpu:1
#SBATCH -p exercise
#SBATCH -o ex5.2_test.txt

echo "start ......"


block_dim=$1
matrix_size=$2

echo "block_dim = $block_dim" 
echo "matrix_size = $matrix_size" 

echo "test matMul >>>>>>>>>>>>>>>>>>>>>>"
# ./bin/matMul -t $block_dim  
./bin/matMul -shared true -print-matrix true -s $matrix_size -t $block_dim
# -c false


# Varying the Problem  Size
# echo " >>>>>>>>>> s 8"
# ./bin/matMul -shared true -print-matrix true -s 8 -t 16
#
# echo " >>>>>>>>>> s 32"
# ./bin/matMul -shared true -print-matrix true -s 32 -t 16
#
# echo " >>>>>>>>>> s 64"
# ./bin/matMul -shared true -print-matrix true -s 64 -t 16
#
# echo " >>>>>>>>>> s 256"
# ./bin/matMul -shared true -print-matrix true -s 256 -t 16
#
# echo " >>>>>>>>>> s 512"
# ./bin/matMul -shared true -print-matrix true -s 512 -t 16
#
# echo " >>>>>>>>>> s 1024"
# ./bin/matMul -shared true -print-matrix true -s 1024 -t 16
#
# echo " >>>>>>>>>> s 2048"
# ./bin/matMul -shared true -print-matrix true -s 2048 -t 16




# Find Optimal Block Size
# echo " >>>>>>>>>> block_dim = 1"
# ./bin/matMul -shared true -print-matrix true -s 2048 -t 1
# 
# echo " >>>>>>>>>> block_dim = 4"
# ./bin/matMul -shared true -print-matrix true -s 2048 -t 4
# 
# echo " >>>>>>>>>> block_dim = 8"
# ./bin/matMul -shared true -print-matrix true -s 2048 -t 8
# 
# echo " >>>>>>>>>> block_dim = 16"
# ./bin/matMul -shared true -print-matrix true -s 2048 -t 16
# 
# echo " >>>>>>>>>> block_dim = 20"
# ./bin/matMul -shared true -print-matrix true -s 2048 -t 20
# 
# echo " >>>>>>>>>> block_dim = 28"
# ./bin/matMul -shared true -print-matrix true -s 2048 -t 28
# 
# echo " >>>>>>>>>> block_dim = 32"
# ./bin/matMul -shared true -print-matrix true -s 2048 -t 32



echo "end ......"






