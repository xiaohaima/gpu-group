#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -o 7_1.csv
#SBATCH -p dev

bin/7_1 -sar csv --benchmark-samples=50
