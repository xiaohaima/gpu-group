// run with: ./bin/7_1 -s -r csv
#include <driver_types.h>
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

#include <cuda_ptrs.hpp>
#include <cooperative_groups.h>

#include "body.hpp"

#include <cstdint>

using namespace cuda_helpers;

// real naive SOA
struct Bodies
{
  float* bodyPosX;
  float* bodyPosY;
  float* bodyPosZ;
  float* bodyMass;
  float* bodySpeedX;
  float* bodySpeedY;
  float* bodySpeedZ;
};

//
// Calculate the Distance of two points
// 9 flops
__device__ float
getDistance(const Bodies &bs, const std::uint32_t bodyIdxA, const std::uint32_t bodyIdxB)
{
  const float xD = bs.bodyPosX[bodyIdxA] - bs.bodyPosX[bodyIdxB];
  const float yD = bs.bodyPosY[bodyIdxA] - bs.bodyPosY[bodyIdxB];
  const float zD = bs.bodyPosZ[bodyIdxA] - bs.bodyPosZ[bodyIdxB];
  return sqrtf(xD*xD + yD*yD + zD*zD);
}

//
// Calculate the forces between two bodies
// 11 flops
__device__ void
bodyBodyInteraction(const Bodies &bs, const std::uint32_t bodyIdxA, const std::uint32_t bodyIdxB, float& fX, float& fY, float& fZ)
{
	const float distance = getDistance(bs, bodyIdxA, bodyIdxB);

	if (distance == 0)
    return;
  
  const float f = -nbody::GAMMA * (bs.bodyMass[bodyIdxA] * bs.bodyMass[bodyIdxB]) / (distance*distance*distance);
  fX = f * (bs.bodyPosX[bodyIdxA] - bs.bodyPosX[bodyIdxB]);
  fY = f * (bs.bodyPosY[bodyIdxA] - bs.bodyPosY[bodyIdxB]);
  fZ = f * (bs.bodyPosZ[bodyIdxA] - bs.bodyPosZ[bodyIdxB]);
}

//
// Calculate the new velocity of one particle
// 7 flops
__device__ void
calculateSpeed(float mass, float fX, float fY, float fZ, float &speedX, float &speedY, float &speedZ)
{
  mass *= nbody::TIMESTEP;
	speedX += fX / mass;
  speedY += fY / mass;
  speedZ += fZ / mass;
}

//
// n-Body Kernel for the speed calculation
//
__global__ void
naiveNbody_Kernel(const std::uint32_t numElements, Bodies bodies)
{
	const std::uint32_t elementId = blockIdx.x * blockDim.x + threadIdx.x;

	if (elementId < numElements) {
    float fX = 0, fY = 0, fZ = 0;
		for (std::uint32_t i = 0; i < numElements; i++) {
			if (i != elementId) {
				bodyBodyInteraction(bodies, elementId, i, fX, fY, fZ);
			}
		}

    float sX = bodies.bodySpeedX[elementId], sY = bodies.bodySpeedY[elementId], sZ = bodies.bodySpeedZ[elementId];
		calculateSpeed(bodies.bodyMass[elementId], fX, fY, fZ, sX, sY, sZ);

    bodies.bodySpeedX[elementId] = sX;
    bodies.bodySpeedY[elementId] = sY;
    bodies.bodySpeedZ[elementId] = sZ;
	}
}

//
// n-Body Kernel to update the position
// Neended to prevent write-after-read-hazards
// 6 flops
__global__ void
updatePosition_Kernel(const std::uint32_t numElements, Bodies bodies)
{
	const std::uint32_t elementId = blockIdx.x * blockDim.x + threadIdx.x;

  if (elementId < numElements) {
    bodies.bodyPosX[elementId] += bodies.bodySpeedX[elementId] * nbody::TIMESTEP;
    bodies.bodyPosY[elementId] += bodies.bodySpeedY[elementId] * nbody::TIMESTEP;
    bodies.bodyPosZ[elementId] += bodies.bodySpeedZ[elementId] * nbody::TIMESTEP;
	}
}

void init_bodies(const std::size_t N,
  HostMemory<float> &posX, HostMemory<float> &posY, HostMemory<float> &posZ,
  HostMemory<float> &mass,
  HostMemory<float> &speedX, HostMemory<float> &speedY, HostMemory<float> &speedZ)
{
  std::mt19937 rnd{2007};
  std::uniform_real_distribution<float> d{0.f, .1f};
  for(std::size_t i = 0; i < N; i++)
  {
    posX[i] = 1e-8 * d(rnd);
    posY[i] = 1e-8 * d(rnd);
    posZ[i] = 1e-8 * d(rnd);
    mass[i] = 1e4 * d(rnd);
    speedX[i] = 0.f;
    speedY[i] = 0.f;
    speedZ[i] = 0.f;
  }
}

SCENARIO("7.1 naive nbody", "[gpu]")
{
  const std::uint64_t N = GENERATE(logRange(2, (1ULL << 15) + 1ULL, 2));
  const std::uint64_t iterations = 100;
  const std::uint64_t flops = N * (7 + (N - 1) * 20 + 6) * iterations;
  const std::uint64_t threadsPerBlock = GENERATE(logRange(32, 1025, 2));

  const std::uint64_t blocks = (N + threadsPerBlock - 1UL) / threadsPerBlock;

  CAPTURE(N);
  CAPTURE(iterations);
  CAPTURE(flops);
  CAPTURE(threadsPerBlock);
  SUCCEED();

  auto bodyPosX = AllocPinnedHost<float>(N);
  auto bodyPosY = AllocPinnedHost<float>(N);
  auto bodyPosZ = AllocPinnedHost<float>(N);
  auto bodyMass = AllocPinnedHost<float>(N);
  auto bodySpeedX = AllocPinnedHost<float>(N);
  auto bodySpeedY = AllocPinnedHost<float>(N);
  auto bodySpeedZ = AllocPinnedHost<float>(N);

  init_bodies(N, bodyPosX, bodyPosY, bodyPosZ, bodyMass, bodySpeedX, bodySpeedY, bodySpeedZ);

  auto dBodyPosX = AllocDeviceMemory<float>(N);
  auto dBodyPosY = AllocDeviceMemory<float>(N);
  auto dBodyPosZ = AllocDeviceMemory<float>(N);
  auto dBodyMass = AllocDeviceMemory<float>(N);
  auto dBodySpeedX = AllocDeviceMemory<float>(N);
  auto dBodySpeedY = AllocDeviceMemory<float>(N);
  auto dBodySpeedZ = AllocDeviceMemory<float>(N);
  Bodies bodies{dBodyPosX.get(), dBodyPosY.get(), dBodyPosZ.get(), dBodyMass.get(), dBodySpeedX.get(), dBodySpeedY.get(), dBodySpeedZ.get()};

  REQUIRE(cudaMemcpy(dBodyPosX.get(), bodyPosX.get(), N * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess);
  REQUIRE(cudaMemcpy(dBodyPosY.get(), bodyPosY.get(), N * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess);
  REQUIRE(cudaMemcpy(dBodyPosZ.get(), bodyPosZ.get(), N * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess);
  REQUIRE(cudaMemcpy(dBodyMass.get(), bodyMass.get(), N * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess);
  REQUIRE(cudaMemcpy(dBodySpeedX.get(), bodySpeedX.get(), N * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess);
  REQUIRE(cudaMemcpy(dBodySpeedY.get(), bodySpeedY.get(), N * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess);
  REQUIRE(cudaMemcpy(dBodySpeedZ.get(), bodySpeedZ.get(), N * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess);

  BENCHMARK("naive nbody"){
    for(std::uint32_t i = 0; i < iterations; ++i)
    {
      naiveNbody_Kernel<<<blocks, threadsPerBlock>>>(N, bodies);
      updatePosition_Kernel<<<blocks, threadsPerBlock>>>(N, bodies);
    }
    REQUIRE(cudaDeviceSynchronize() == cudaSuccess);
  };
}
