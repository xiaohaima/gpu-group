#pragma once
#include <cuda_ptrs.hpp>

namespace nbody
{

static constexpr float TIMESTEP = 1e-6;
static constexpr float GAMMA = 6.673e-11;

//
// Structures
//
// Use a SOA (Structure of Arrays)
//
struct Body {
	float4* posMass;  /* x = x */
	                  /* y = y */
	                  /* z = z */
	                  /* w = Mass */
	float3* velocity; /* x = v_x*/
	                  /* y = v_y */
	                  /* z= v_z */
	
	Body()
    : posMass(NULL)
    , velocity(NULL) {}
	Body(float4* pM, float3* v) : posMass(pM), velocity(v) {}
	};

//
// Function Prototypes
//
void printElement(Body, int, int);

//
// Device Functions
//

//
// Calculate the Distance of two points
//
__device__ float
getDistance(float4 a, float4 b)
{
  const float xD = a.x - b.x;
  const float yD = a.y - b.y;
  const float zD = a.z - b.z;
  return sqrtf(xD*xD + yD*yD + zD*zD);
}

//
// Calculate the forces between two bodies
//
__device__ void
bodyBodyInteraction(float4 bodyA, float4 bodyB, float3& force)
{
	const float distance = getDistance(bodyA, bodyB);

	if (distance==0) 
		return;
  
  const float f = -GAMMA * (bodyA.w * bodyB.w) / distance;
  force.x += f * (bodyA.x - bodyB.x);
  force.y += f * (bodyA.y - bodyB.y);
  force.z += f * (bodyA.z - bodyB.z);
}

//
// Calculate the new velocity of one particle
//
__device__ void
calculateSpeed(float mass, float3& currentSpeed, float3 force)
{
	const float3 a = make_float3(force.x / mass, force.y / mass, force.z / mass);
	currentSpeed.x += a.x * TIMESTEP;
  currentSpeed.y += a.y * TIMESTEP;
  currentSpeed.z += a.z * TIMESTEP;
}


}
