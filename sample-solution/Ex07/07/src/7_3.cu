// run with: ./bin/7_1 -s -r csv
#include <algorithm>
#include <array>
#include <cstdint>
#include <cuda_device_runtime_api.h>
#include <driver_types.h>
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

#include <numeric>

#include <cooperative_groups.h>
#include <cuda_ptrs.hpp>

#include "body.hpp"

using namespace cuda_helpers;

//
// n-Body Kernel to update the position
// Neended to prevent write-after-read-hazards
//
__global__ void updatePosition_Kernel(int numElements, float4 *bodyPos,
                                      float3 *bodySpeed) // Joachim
{
  const std::uint32_t elementId = blockIdx.x * blockDim.x + threadIdx.x;

  if (elementId < numElements) {
    float4 elementPosMass = bodyPos[elementId];
    const float3 elementSpeed = bodySpeed[elementId];
    elementPosMass.x += elementSpeed.x * nbody::TIMESTEP;
    elementPosMass.y += elementSpeed.y * nbody::TIMESTEP;
    elementPosMass.z += elementSpeed.z * nbody::TIMESTEP;
    bodyPos[elementId] = elementPosMass;
  }
}

__global__ void streamingNbody_Kernel(const std::uint32_t numElements,
                                      const float4 *bodyPosMass,
                                      const std::uint32_t numElementsOther,
                                      const float4 *bodyPosMassOther,
                                      float3 *bodySpeed) {
  // computes the bodySpeeds for the bodys pointed to by bodyPosMass that are
  // caused by the bodys pointed to by bodyPosMassOther
  // bodyPosMass and bodyPosMassOther can be the same
  const std::uint32_t elementId = blockIdx.x * blockDim.x + threadIdx.x;

  if (elementId < numElements) {
    const float4 elementPosMass = bodyPosMass[elementId];
    float3 elementSpeed = bodySpeed[elementId];
    float3 elementForce = make_float3(0, 0, 0);

    for (int i = 0; i < numElementsOther; i++) {
      if (bodyPosMassOther + i == bodyPosMass + elementId)
        continue;
      nbody::bodyBodyInteraction(elementPosMass, bodyPosMassOther[i],
                                 elementForce);
    }

    nbody::calculateSpeed(elementPosMass.w, elementSpeed, elementForce);

    bodySpeed[elementId] = elementSpeed;
  }
}

//
// n-Body Kernel for the speed calculation (same as previous exercise, for reference only)
//
__global__ void
simpleNbody_Kernel(const std::uint32_t numElements, float4* bodyPosMass, float3* bodySpeed)
{
	const std::uint32_t elementId = blockIdx.x * blockDim.x + threadIdx.x;

	if (elementId < numElements) {
		const float4 elementPosMass = bodyPosMass[elementId] ;
		float3 elementSpeed = bodySpeed[elementId];
		float3 elementForce = make_float3(0,0,0);

		for (int i = 0; i < numElements; i++) {
			if (i != elementId) {
				nbody::bodyBodyInteraction(elementPosMass, bodyPosMass[i], elementForce);
			}
		}

		nbody::calculateSpeed(elementPosMass.w, elementSpeed, elementForce);

		bodySpeed[elementId] = elementSpeed;
	}
}

void init_bodies(const std::size_t N, HostMemory<float4> &bodyPosMass,
                 HostMemory<float3> &bodySpeed) {
  std::mt19937 rnd{2007};
  std::uniform_real_distribution<float> d{0.f, .1f};
  for (std::size_t i = 0; i < N; i++) {
    bodyPosMass[i].x = 1e-8 * d(rnd);
    bodyPosMass[i].y = 1e-8 * d(rnd);
    bodyPosMass[i].z = 1e-8 * d(rnd);
    bodyPosMass[i].w = 1e4 * d(rnd);
    bodySpeed[i].x = 0.f;
    bodySpeed[i].y = 0.f;
    bodySpeed[i].z = 0.f;
  }
}

SCENARIO("7.3 streaming nbody", "[gpu]") {
  const std::uint64_t N = GENERATE(logRange(2, (1ULL << 15) + 1ULL, 2));
  const std::uint64_t iterations = 100;
  const std::uint64_t flops = N * sizeof(float) * 33 * iterations;
  const std::uint64_t threadsPerBlock = GENERATE(logRange(32, 1025, 2));

  // we have memory budget of 4MB
  constexpr std::uint64_t memoryBudget = 4'000'000; // Bytes
  // we want two streams to operate at the same time, each working on one chunk
  // of data
  constexpr std::uint64_t numberOfStreams = 2;
  const std::uint64_t memoryPerStream = memoryBudget / numberOfStreams;
  // 4MB and 4 Byte per float means we can store 1'000'000 floats in total and
  // 500'000 per stream
  const std::uint64_t flotsPerStream = memoryPerStream / sizeof(float);
  // for each body interaction we need a total of 11 floats (4 for each of the
  // involved bodys and 3 for the result)
  const std::uint64_t floatsPerInteraction = 11;
  // this means we can do 500'000/11 = 45'454 body computations per chunk
  const std::uint64_t chunksize = flotsPerStream / floatsPerInteraction;

  const std::uint64_t NumberOfChunks = (N + chunksize - 1) / chunksize;

  const std::uint64_t blocks =
      (chunksize + threadsPerBlock - 1UL) / threadsPerBlock;
  const std::uint64_t gridSize =
      ceil(static_cast<float>(chunksize) / static_cast<float>(blocks));

  dim3 grid_dim = dim3(gridSize);
  dim3 block_dim = dim3(blocks);

  CAPTURE(N);
  CAPTURE(iterations);
  CAPTURE(flops);
  CAPTURE(threadsPerBlock);
  SUCCEED();

  // Host Memory
  auto bodyPosMass = AllocPinnedHost<float4>(N);
  auto bodySpeed = AllocPinnedHost<float3>(N);

  init_bodies(N, bodyPosMass, bodySpeed);

  bool compareResult = N < 129;
  std::unique_ptr<float4[], std::function<void(float4*)>>  bodyPosMassReference = nullptr;

  if(compareResult) {
      bodyPosMassReference = AllocPinnedHost<float4>(N);
      std::memcpy(bodyPosMassReference.get(),bodyPosMass.get(),N * sizeof(float4));
  }

  // we need to perform the computation with every combination of chunks
  // fixed chunk gets replaced in the outer for loop, varying chunk in the
  // inner for loop

  // Device Memory
  std::array<DeviceMemory<float4>, numberOfStreams> fixedChunk{
      AllocDeviceMemory<float4>(chunksize),
      AllocDeviceMemory<float4>(chunksize)};
  std::array<DeviceMemory<float4>, numberOfStreams> varyingChunk{
      AllocDeviceMemory<float4>(chunksize),
      AllocDeviceMemory<float4>(chunksize)};
  std::array<DeviceMemory<float3>, numberOfStreams> bodySpeedChunk{
      AllocDeviceMemory<float3>(chunksize),
      AllocDeviceMemory<float3>(chunksize)};

  // Streams
  std::array<cudaStream_t, numberOfStreams> streams;

  for (auto &i : streams) {
    REQUIRE(cudaStreamCreate(&i) == cudaSuccess);
  }

  BENCHMARK("nbody with streaming") {

    for (std::uint32_t iter = 0; iter < iterations; iter++) {
      // iterate over the chunks (fixed chunk) to calculate the speeds
      for (int i = 0; i < NumberOfChunks; i++) {
        int streamIdx = i % numberOfStreams;
        int currentFixedChunkSize = std::min(
            chunksize, N - (i * chunksize)); // only relevant for the last chunk
        // copy fixed chunk to device
        cudaMemcpyAsync(
            fixedChunk[streamIdx].get(), bodyPosMass.get() + i * chunksize,
            currentFixedChunkSize * sizeof(float4),
            cudaMemcpyKind::cudaMemcpyHostToDevice, streams[streamIdx]);
        // set memory that conatains the speed to zero
        cudaMemsetAsync(bodySpeedChunk[streamIdx].get(), 0.f,
                        currentFixedChunkSize * sizeof(float3),
                        streams[streamIdx]);
        // interaction within fixed chunk
        streamingNbody_Kernel<<<grid_dim, block_dim, 0, streams[streamIdx]>>>(
            currentFixedChunkSize, fixedChunk[streamIdx].get(),
            currentFixedChunkSize, fixedChunk[streamIdx].get(),
            bodySpeedChunk[streamIdx].get() + (chunksize * i));
        // iterate over varying chunk
        for (int j = 0; j > NumberOfChunks; j++) {
          if (i == j)
            continue;
          int currentVaryingChunkSize =
              std::min(chunksize,
                       N - (j * chunksize)); // only relevant for the last chunk
          // copy varying chunk to device
          cudaMemcpyAsync(
              varyingChunk[streamIdx].get(), bodyPosMass.get() + i * chunksize,
              currentVaryingChunkSize * sizeof(float4),
              cudaMemcpyKind::cudaMemcpyHostToDevice, streams[streamIdx]);
          streamingNbody_Kernel<<<grid_dim, block_dim, 0, streams[streamIdx]>>>(
              currentFixedChunkSize, fixedChunk[streamIdx].get(),
              currentVaryingChunkSize * sizeof(float4),
              varyingChunk[streamIdx].get(),
              bodySpeedChunk[streamIdx].get() + (chunksize * i));
        }
        // copy results back to host
        cudaMemcpyAsync(bodySpeed.get() + (chunksize * i),
                        bodySpeedChunk[streamIdx].get(),
                        currentFixedChunkSize * sizeof(float3),
                        cudaMemcpyDeviceToHost, streams[streamIdx]);
      }
      cudaDeviceSynchronize();
      // iterate over the chunks (fixed chunk) to update positions
      for (int i = 0; i < NumberOfChunks; i++) {
        int streamIdx = i % numberOfStreams;
        int currentFixedChunkSize = std::min(
            chunksize, N - (i * chunksize)); // only relevant for the last chunk
        // copy fixed chunk to device
        cudaMemcpyAsync(
            fixedChunk[streamIdx].get(), bodyPosMass.get() + i * chunksize,
            currentFixedChunkSize * sizeof(float4),
            cudaMemcpyKind::cudaMemcpyHostToDevice, streams[streamIdx]);
        cudaMemcpyAsync(bodySpeedChunk[streamIdx].get(),
                        bodySpeed.get() + i * chunksize,
                        currentFixedChunkSize * sizeof(float4),
                        cudaMemcpyHostToDevice, streams[streamIdx]);
        updatePosition_Kernel<<<grid_dim, block_dim, 0, streams[streamIdx]>>>(
            currentFixedChunkSize, fixedChunk[streamIdx].get(),
            bodySpeedChunk[streamIdx].get());
      }
      cudaDeviceSynchronize();
    }
    REQUIRE(cudaDeviceSynchronize() == cudaSuccess);
  };

  if(compareResult && bodyPosMassReference != nullptr)
  {
    // Device Memory
	auto dBodyPosMassReference = AllocDeviceMemory<float4>(N);
	auto dBodySpeedReference = AllocDeviceMemory<float3>(N);

    cudaMemcpy(dBodyPosMassReference.get(), bodyPosMassReference.get(), static_cast<size_t>(N * sizeof(float4)), cudaMemcpyHostToDevice);
    cudaMemset(dBodySpeedReference.get(), 0.f, static_cast<size_t>(N * sizeof(float3)));
    
    for (std::uint32_t i = 0; i < iterations; ++i)
    {
        const std::uint64_t blocks = (N + threadsPerBlock - 1UL) / threadsPerBlock;
        const std::uint64_t gridSize = ceil(static_cast<float>(N) / static_cast<float>(blocks));
    
        dim3 grid_dim = dim3(gridSize);
        dim3 block_dim = dim3(blocks);
        simpleNbody_Kernel <<<grid_dim, block_dim >>> (N, dBodyPosMassReference.get(), dBodySpeedReference.get());
        updatePosition_Kernel <<<grid_dim, block_dim >>> (N, dBodyPosMassReference.get(), dBodySpeedReference.get());
    }

    auto BodySpeedReference = AllocPinnedHost<float3>(N);
    cudaMemcpy(BodySpeedReference.get(),dBodySpeedReference.get(),N*sizeof(float3),cudaMemcpyDeviceToHost);

    CHECK(memcmp(BodySpeedReference.get(),bodySpeed.get(),N*sizeof(float3)) == 0);
  }
}