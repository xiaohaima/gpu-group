// run with: ./bin/7_1 -s -r csv
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

#include <numeric>

#include <cuda_ptrs.hpp>
#include <cooperative_groups.h>

#include "body.hpp"

#include <cstdint>

using namespace cuda_helpers;


//
// n-Body Kernel for the speed calculation
//
__global__ void
simpleNbody_Kernel(const std::uint32_t numElements, float4* bodyPosMass, float3* bodySpeed)					
{
	const std::uint32_t elementId = blockIdx.x * blockDim.x + threadIdx.x;

	if (elementId < numElements) {
		const float4 elementPosMass = bodyPosMass[elementId] ;
		float3 elementSpeed = bodySpeed[elementId];
		float3 elementForce = make_float3(0,0,0);

		for (int i = 0; i < numElements; i++) {
			if (i != elementId) {
				nbody::bodyBodyInteraction(elementPosMass, bodyPosMass[i], elementForce);
			}
		}

		nbody::calculateSpeed(elementPosMass.w, elementSpeed, elementForce);

		bodySpeed[elementId] = elementSpeed;
	}
}

__global__ void
sharedNbody_Kernel(const std::uint32_t numElements, const float4* bodyPosMass, float3* bodySpeed)
{
	extern __shared__ float4 shPosMass[];							//shared memory on device
	

	const std::uint32_t elementId = blockIdx.x * blockDim.x + threadIdx.x;

	if (elementId < numElements)
	{
			float4 myPosMass = bodyPosMass[elementId];							
			float4 elementPosMass;
			float3 elementForce = make_float3(0, 0, 0);

			#pragma unroll
			for (int j = threadIdx.x; j < numElements; j += blockDim.x)					//outer loop that strides through bodies 
			{
				shPosMass[threadIdx.x] = bodyPosMass[j];
				__syncthreads();

				for (int k = 0; k < blockDim.x && k < numElements; k++)					//Inner loop that iterates over body descriptions (1 block = 1 body)
				{
					if (k != elementId)
					{
						elementPosMass = shPosMass[k];
						nbody::bodyBodyInteraction(elementPosMass, myPosMass, elementForce);
					}
				}
				__syncthreads();
				
			}
			float3 elementSpeed = bodySpeed[elementId];
			nbody::calculateSpeed(elementPosMass.w, elementSpeed, elementForce);

			bodySpeed[elementId] = elementSpeed;
	}	
}


//
// n-Body Kernel to update the position
// Neended to prevent write-after-read-hazards
//
__global__ void
updatePosition_Kernel(int numElements, float4* bodyPos, float3* bodySpeed)			
{
	const std::uint32_t elementId = blockIdx.x * blockDim.x + threadIdx.x;

  if (elementId < numElements) {
    float4 elementPosMass = bodyPos[elementId];
    const float3 elementSpeed = bodySpeed[elementId];
    elementPosMass.x += elementSpeed.x * nbody::TIMESTEP;
    elementPosMass.y += elementSpeed.y * nbody::TIMESTEP;
    elementPosMass.z += elementSpeed.z * nbody::TIMESTEP;
    bodyPos[elementId] = elementPosMass;
	}
}



void init_bodies(const std::size_t N, HostMemory<float4> &bodyPosMass, HostMemory<float3> &bodySpeed)
{
	std::mt19937 rnd{ 2007 };
	std::uniform_real_distribution<float> d{ 0.f, .1f };
	for (std::size_t i = 0; i < N; i++)
	{
		bodyPosMass[i].x = 1e-8 * d(rnd);
		bodyPosMass[i].y = 1e-8 * d(rnd);
		bodyPosMass[i].z = 1e-8 * d(rnd);
		bodyPosMass[i].w = 1e4 * d(rnd);
		bodySpeed[i].x = 0.f;
		bodySpeed[i].y = 0.f;
		bodySpeed[i].z = 0.f;
	}
}


SCENARIO("7.2 shared nbody", "[gpu]")
{
	const std::uint64_t N = GENERATE(logRange(2, (1ULL << 15) + 1ULL, 2));
	const std::uint64_t iterations = 100;
	const std::uint64_t flops = N * (7 + (N - 1) * 20 + 6) * iterations;
	const std::uint64_t threadsPerBlock = GENERATE(logRange(32, 1025, 2));

	const std::uint64_t blocks = (N + threadsPerBlock - 1UL) / threadsPerBlock;


	CAPTURE(N);
	CAPTURE(iterations);
	CAPTURE(flops);
	CAPTURE(threadsPerBlock);
	SUCCEED();


	//Host Memory
	auto bodyPosMass = AllocPinnedHost<float4>(N);
	auto bodySpeed = AllocPinnedHost<float3>(N);
	
	init_bodies(N, bodyPosMass, bodySpeed);

	

	// Device Memory
	auto dBodyPosMass = AllocDeviceMemory<float4>(N);
	auto dBodySpeed = AllocDeviceMemory<float3>(N);

	nbody::Body particles{dBodyPosMass.get(), dBodySpeed.get()};

	
	REQUIRE(cudaMemcpy(dBodyPosMass.get(), bodyPosMass.get(), static_cast<size_t>(N * sizeof(float4)), cudaMemcpyHostToDevice) == cudaSuccess);
	REQUIRE(cudaMemcpy(dBodySpeed.get(), bodySpeed.get(), static_cast<size_t>(N * sizeof(float3)), cudaMemcpyHostToDevice) == cudaSuccess);

	BENCHMARK("nbody with packed data types") {
		for (std::uint32_t i = 0; i < iterations; ++i)
		{
			simpleNbody_Kernel << <blocks, threadsPerBlock >> > (N, particles.posMass, particles.velocity);
			updatePosition_Kernel << <blocks, threadsPerBlock >> > (N, particles.posMass, particles.velocity);
		}
		REQUIRE(cudaDeviceSynchronize() == cudaSuccess);
	};

	const int sharedMemoryNeeded = threadsPerBlock * (sizeof(float4));

	BENCHMARK("nbody with packed data and SM")
	{
		for (std::uint32_t i = 0; i < iterations; i++)
		{
			sharedNbody_Kernel <<<blocks, threadsPerBlock, sharedMemoryNeeded >>> (N, particles.posMass, particles.velocity);
			updatePosition_Kernel << <blocks, threadsPerBlock >> > (N, particles.posMass, particles.velocity);
		}
		REQUIRE(cudaDeviceSynchronize() == cudaSuccess);
	};
}