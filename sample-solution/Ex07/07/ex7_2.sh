#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -o 7_2.csv
#SBATCH -p dev

bin/7_2 -sar csv --benchmark-samples=50
