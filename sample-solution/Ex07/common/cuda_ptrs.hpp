#pragma once
#include <cstdint>
#include <memory>
#include <stdexcept>
#include <string>
#include <functional>

#include <cuda_runtime.h>

namespace cuda_helpers
{
template <class T>
[[nodiscard]] std::unique_ptr<T[], std::function<void(T*)>> AllocPinnedHost(std::size_t count)
{
  T *ptr = nullptr;
  if(cudaMallocHost(&ptr, sizeof(T) * count) != cudaSuccess)
    throw std::runtime_error{std::string{"cudaMallocHost failed with "} + std::to_string(cudaGetLastError())};
  return std::unique_ptr<T[], std::function<void(T*)>>{ptr, [](T *toDelete ) { cudaFreeHost(toDelete); }};
}

template <class T>
[[nodiscard]] std::unique_ptr<T[], std::function<void(T*)>> AllocUnpinnedHost(std::size_t count)
{
  T *ptr = reinterpret_cast<T*>(malloc(sizeof(T) * count));
  return std::unique_ptr<T[], std::function<void(T*)>>{ptr, [](T *toDelete) { free(toDelete); }};
}

template <class T>
[[nodiscard]] auto AllocDeviceMemory(std::size_t count)
{
  T *ptr = nullptr;
  if(cudaMalloc(&ptr, sizeof(T) * count) != cudaSuccess)
    throw std::runtime_error{std::string{"cudaMalloc failed with "} + std::to_string(cudaGetLastError())};
  auto deleter = [](T *toDelete) { cudaFree(toDelete); };
  return std::unique_ptr<T, decltype(deleter)>{ptr, std::move(deleter)};
}

template <class T>
using HostMemory = std::unique_ptr<T[], std::function<void(T*)>>;
template <class T>
using PinnedMemory = HostMemory<T>;
template <class T>
using UnpinnedMemory = HostMemory<T>;
template <class T>
using DeviceMemory = decltype(AllocDeviceMemory<T>(1));
} // namespace cuda_helpers
