#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -o 6_2_3.csv
#SBATCH -p dev

bin/6_2_3 -sar csv [gpu] --benchmark-samples=50 >> 6_2_3gpu.csv
bin/6_2_3 -sar csv [cpu] --benchmark-samples=50 >> 6_2_3cpu.csv
