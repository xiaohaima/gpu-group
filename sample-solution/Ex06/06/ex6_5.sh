#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -o 6_5.csv
#SBATCH -p dev

bin/6_5 [gpu] -sar csv  --benchmark-samples=50 >> 6_5_gpu_naive.csv
bin/6_5 [thrust] -sar csv  --benchmark-samples=50 >> 6_5_thrust.csv


