// run with: ./bin/6_4 -s -r csv
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

#include <numeric>

#include <cuda_ptrs.hpp>
#include <cooperative_groups.h>

using namespace cuda_helpers;

// using cooperative groups, so we don't really have to care about Volta
// and dynamic warps and so on.. some docs:
// https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#cooperative-groups
// https://developer.nvidia.com/blog/cooperative-groups/
namespace cg = cooperative_groups;

__global__ void reduce_gpu_kernel(const std::size_t N, const float* in, float* out)
{
  extern __shared__ float sPartial[];

  auto grid = cg::this_grid();
  auto block = cg::this_thread_block();

  float localPartial = 0;
  // need a loop as we have to load everything, even if it doesn't fit inside the grid
  for(std::size_t i = grid.thread_rank(); i < N; i += grid.size())
    localPartial += in[i];

  sPartial[block.thread_rank()] = localPartial;

  block.sync();

  for(int i = block.size()/2; i > 1; i /= 2)
  {
    if(block.thread_rank() < i)
    {
      sPartial[block.thread_rank()] += sPartial[block.thread_rank() + i];
    }
    block.sync();
  }

  //unrolling the last iterations implicitly relies on the 32 threads of a warp being sycnchronized, so it might lead to problems in newer GPUs

  if(block.thread_rank() == 0)
    out[block.group_index().x] = sPartial[0];
}

void init_vector(const std::size_t N, HostMemory<float> &in)
{
  std::mt19937 rnd{2007};
  std::uniform_real_distribution<float> d{0.f, .1f};
  for(std::size_t i = 0; i < N; i++)
  {
    // in[i] = d(rnd);
    in[i] = 0.1f;
  }
}

SCENARIO("6.4 reduce GPU", "[gpu]")
{
  const std::uint64_t N = GENERATE(logRange(2, (1ULL << 30) + 1ULL, 2));
  const std::uint64_t bytes = N * sizeof(float);
  const std::uint64_t threadsPerBlock = GENERATE(logRange(32, 1025, 2));
  const std::uint64_t MaxVerify = N;

  const std::uint64_t blocks = std::min((N + threadsPerBlock - 1UL) / threadsPerBlock, threadsPerBlock);
  const std::uint64_t threads2ndStep = std::min(((blocks + 31) / 32) * 32UL, threadsPerBlock);

  CAPTURE(N);
  CAPTURE(bytes);
  CAPTURE(threadsPerBlock);
  SUCCEED();

  auto in = AllocPinnedHost<float>(N);
  auto gpuOut = AllocPinnedHost<float>(1);

  init_vector(N, in);

  auto dIn = AllocDeviceMemory<float>(N);
  auto dOut = AllocDeviceMemory<float>(blocks); // used as intermediate and out

  REQUIRE(cudaMemcpy(dIn.get(), in.get(), N * sizeof(float), cudaMemcpyHostToDevice) == cudaSuccess);

  if(N <= MaxVerify)
  {
    reduce_gpu_kernel<<<blocks, threadsPerBlock, threadsPerBlock * sizeof(float)>>>(N, dIn.get(), dOut.get());
    reduce_gpu_kernel<<<1, threads2ndStep, threads2ndStep * sizeof(float)>>>(blocks, dOut.get(), dOut.get());
    REQUIRE(cudaMemcpyAsync(gpuOut.get(), dOut.get(), sizeof(float), cudaMemcpyDeviceToHost) == cudaSuccess);
    REQUIRE(cudaDeviceSynchronize() == cudaSuccess);
    // use std accumulate directly using doubles here, instead of floats to get somewhat better precision..
    CHECK(gpuOut[0] == Approx(std::accumulate(in.get(), in.get() + N, 0.0)).epsilon(1));
  }

  BENCHMARK("reduce GPU improved"){
    reduce_gpu_kernel<<<blocks, threadsPerBlock, threadsPerBlock * sizeof(float)>>>(N, dIn.get(), dOut.get());
    reduce_gpu_kernel<<<1, threads2ndStep, threads2ndStep * sizeof(float)>>>(blocks, dOut.get(), dOut.get());
    cudaDeviceSynchronize();
  };
}
