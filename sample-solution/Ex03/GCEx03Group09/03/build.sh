#!/usr/bin/env bash

# gets a recent cmake and runs the build..

if [ ! -d "build" ]; then
    mkdir build
fi

cd build

if [ ! -d "cmake-3.18.4-Linux-x86_64/bin/" ]; then
    echo "Installing recent CMake.."
    wget https://github.com/Kitware/CMake/releases/download/v3.18.4/cmake-3.18.4-Linux-x86_64.sh
    chmod u+x cmake-3.18.4-Linux-x86_64.sh
    ./cmake-3.18.4-Linux-x86_64.sh --prefix=. --skip-license --include-subdir
fi
cmake-3.18.4-Linux-x86_64/bin/cmake -DCMAKE_BUILD_TYPE=Release ..
cmake-3.18.4-Linux-x86_64/bin/cmake --build . -j

