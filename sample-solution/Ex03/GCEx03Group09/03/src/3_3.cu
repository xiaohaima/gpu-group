// run with: ./bin/3_3 -s -r csv --benchmark-samples=10
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

__global__ void
globalMemCoalescedKernel(const int* beginSource, int count, int* beginTarget)
{
    int const offset = blockIdx.x * blockDim.x + threadIdx.x;
    if (offset < count) {
        *(beginTarget + offset) = *(beginSource + offset);
    }
}

__global__ void
globalMemStridedKernel(const int* beginSource, int count, int* beginTarget, std::uint64_t stride)
{
    int const offset = blockIdx.x * blockDim.x + threadIdx.x;
    if (offset < count) {
        *(beginTarget + offset) = *(beginSource + offset * stride);
    }
}

SCENARIO("3.3 global strided")
{
    // generate runs with 1 to 64 stride
    const std::uint64_t stride = GENERATE(range(1,65,1));
    const std::uint64_t memorySize = 10*1024*1024; //10 MiB
    const std::uint64_t threadsPerBlock = 1024;
    const std::uint64_t numBlocks = (memorySize + threadsPerBlock - 1) / threadsPerBlock;
    
    CAPTURE(stride);
    CAPTURE(threadsPerBlock);
    CAPTURE(numBlocks);
    CAPTURE(memorySize);
    SUCCEED();

    int* d_memoryA = nullptr;
    int* d_memoryB = nullptr;
    cudaError_t cudaError;
    cudaError = cudaMalloc( &d_memoryA, memorySize * stride);
    REQUIRE (cudaError == cudaSuccess);
    cudaError = cudaMalloc( &d_memoryB, memorySize);
    REQUIRE (d_memoryA != nullptr);
    REQUIRE (d_memoryB != nullptr);
    BENCHMARK("D2D copy with stride")
    {
        globalMemStridedKernel<<<numBlocks, threadsPerBlock>>>(d_memoryA, memorySize / sizeof(int), d_memoryB, stride);
        cudaError = cudaDeviceSynchronize();
    };
    BENCHMARK("D2D copy coalesced")
    {
        globalMemCoalescedKernel<<<numBlocks, threadsPerBlock>>>(d_memoryA, memorySize / sizeof(int), d_memoryB);
        cudaError = cudaDeviceSynchronize();
    };
    CHECK (cudaError == cudaSuccess);

    cudaError = cudaFree(d_memoryA);
    CHECK (cudaError == cudaSuccess);

    cudaError = cudaFree(d_memoryB);
    CHECK (cudaError == cudaSuccess);
}