// run with: ./bin/3_4 -s -r csv --benchmark-samples=10
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

__global__ void
globalMemCoalescedKernel(const int* beginSource, int count, int* beginTarget)                       //coalesced memory access      
{
    int const offset = blockIdx.x * blockDim.x + threadIdx.x;
    if (offset < count) {
        *(beginTarget + offset) = *(beginSource + offset);
    }
}

__global__ void
globalMemOffsetKernel(const int* beginSource, int count, int* beginTarget, std::uint64_t shift)     //Memory access with shift 
{
    int const offset = blockIdx.x * blockDim.x + threadIdx.x;
    if (offset < count) {
        *(beginTarget + offset) = *(beginSource + offset + shift);
    }
}

SCENARIO("3.4 global offset")
{
    // generate runs with offset from 1 to 64 
    const std::uint64_t shift = GENERATE(range(0,65,1));
    const std::uint64_t memorySize = 10*1024*1024; //10 MiB
    const std::uint64_t threadsPerBlock = 1024;
    const std::uint64_t numBlocks = (memorySize + threadsPerBlock - 1) / threadsPerBlock;
    
    CAPTURE(shift);                     //Capturing of offset values for plotting
    CAPTURE(threadsPerBlock);
    CAPTURE(numBlocks);
    CAPTURE(memorySize);
    SUCCEED();

    int* d_memoryA = nullptr;
    int* d_memoryB = nullptr;
    cudaError_t cudaError;
    cudaError = cudaMalloc( &d_memoryA, memorySize + shift * sizeof(int));
    REQUIRE (cudaError == cudaSuccess);
    cudaError = cudaMalloc( &d_memoryB, memorySize);
    REQUIRE (d_memoryA != nullptr);
    REQUIRE (d_memoryB != nullptr);

    BENCHMARK("D2D copy with offset")               //Measurement of time for memory access with shift
    {
        globalMemOffsetKernel<<<numBlocks, threadsPerBlock>>>(d_memoryA, memorySize / sizeof(int), d_memoryB, shift);
        cudaError = cudaDeviceSynchronize();
    };

    BENCHMARK("D2D copy coalesced")                 //Measurement of time for coalesced memory access (reference)
    {
        globalMemCoalescedKernel<<<numBlocks, threadsPerBlock>>>(d_memoryA, memorySize / sizeof(int), d_memoryB);
        cudaError = cudaDeviceSynchronize();        //Synchronize all devices
    };
    CHECK (cudaError == cudaSuccess);               //make sure that task is completed and without errors

    cudaError = cudaFree(d_memoryA);                //free device buffer
    CHECK (cudaError == cudaSuccess);               //make sure that task is completed and without errors

    cudaError = cudaFree(d_memoryB);                //free device buffer
    CHECK (cudaError == cudaSuccess);               //make sure that task is completed and without errors
}