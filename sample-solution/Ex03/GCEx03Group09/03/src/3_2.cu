// run with: ./bin/3_2 -s -r csv --benchmark-samples=10
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

__global__ void
globalMemCoalescedKernel(int* beginSource, int count, int* beginTarget, int numberOfBlocks)
{
    int const offset = blockIdx.x *blockDim.x + threadIdx.x;
    int currElement = offset;
    while (currElement < count) {
        *(beginTarget + currElement) = *(beginSource + currElement);
        currElement += blockDim.x * numberOfBlocks;
    }
}

SCENARIO("3.2 global coalesced")
{
    // generate runs with 1 to 1024 threads per Block
    std::uint64_t threadsPerBlock = GENERATE(logRange(1,1025,16));
    //std::uint64_t threadsPerBlock = 1024;
    std::uint64_t numBlocks = GENERATE(range(1,33,1));
    //std::uint64_t numBlocks = 1;
    CAPTURE(threadsPerBlock);
    CAPTURE(numBlocks);
    std::uint64_t memorySize = 10*1024*1024; //10 MiB
    CAPTURE(memorySize);
    SUCCEED();

    int* d_memoryA = nullptr;
    int* d_memoryB = nullptr;
    cudaError_t cudaError;
    cudaError = cudaMalloc( &d_memoryA, memorySize);
    REQUIRE (cudaError == cudaSuccess);
    cudaError = cudaMalloc( &d_memoryB, memorySize);
    REQUIRE (d_memoryA != nullptr);
    REQUIRE (d_memoryB != nullptr);
    BENCHMARK("D2D copy")
    {
        globalMemCoalescedKernel<<<numBlocks, threadsPerBlock>>>(d_memoryA, memorySize / sizeof(int), d_memoryB, numBlocks);
        cudaError = cudaDeviceSynchronize();
    };
    CHECK (cudaError == cudaSuccess);

    cudaError = cudaFree(d_memoryA);
    CHECK (cudaError == cudaSuccess);

    cudaError = cudaFree(d_memoryB);
    CHECK (cudaError == cudaSuccess);
}