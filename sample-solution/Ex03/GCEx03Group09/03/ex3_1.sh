#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -o memcpy.csv
#SBATCH -p exercise

bin/memcpy -s -r csv --benchmark-samples=50
