// run with: ./bin/4_4 -s -r csv
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

void matrix_multiply(int M,const std::vector<double>& A,const std::vector<double>& B, std::vector<double>& C)
{
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < M; j++)
		{
			for (int k = 0; k < M; k++)
			{
				C[j + i * M] = C[j + i * M] + A[k + i * M] * B[j + k * M];
			}
		}
	}
}

SCENARIO("4.4 MM CPU")
{
	//Define Matrix sizes
	const std::uint64_t M = GENERATE(range(5,2000,10));

	CAPTURE(M);
	SUCCEED();

	std::vector<double> A;
	std::vector<double> B;
	std::vector<double> C;

	A.resize(M*M);
	B.resize(M*M);
	C.resize(M*M);

	for (int y_i = 0; y_i < M; y_i++)							//Filling first matrix (A) with A[i,j] = i+j
	{
		for (int x_i = 0; x_i < M; x_i++)
		{
			A[x_i + y_i * M] = x_i + y_i;
		}
	}

    for (int y_i = 0; y_i < M; y_i++)							//Filling second matrix (B) with B[i,j] = i*j
	{
		for (int x_i = 0; x_i < M; x_i++)
		{
			B[x_i + y_i * M] = x_i * y_i;
		}
	}

    for (int y_i = 0; y_i < M; y_i++)							//Filling third matrix (C) with 0
	{
		for (int x_i = 0; x_i < M; x_i++)
		{
			C[x_i + y_i *M] = 0;
		}
	}

	
		
	BENCHMARK("MM CPU Naive")
	{
		matrix_multiply(M, A, B, C);

	};	
	
}
