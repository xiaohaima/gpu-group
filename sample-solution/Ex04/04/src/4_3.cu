// run with: ./bin/4_2 -s -r csv
#include <cstdint>
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

//count: number of elements per thread Block
__global__ void
readSharedMemory(const int iterations, const int stride, std::uint64_t *clocks)
{
  extern __shared__ int sharedMem[];
  int currElement = threadIdx.x * stride;
  int max = 0;
  std::uint64_t start = clock64();
  for(int i = 0; i < iterations; ++i)
  {
    int currValue = sharedMem[currElement++];
    max = max < currValue ? currValue : max;
  }
  std::uint64_t end = clock64();
  clocks[threadIdx.x] = (end - start) / iterations;

  if(0 == threadIdx.x)
  {
    //this should prevent the compiler from optimizing this kernel to nothing while also not adding a lot of memory accesses
    sharedMem[threadIdx.x] = max;
  }
}

SCENARIO("4.3 shared conflicts")
{
  std::uint64_t stride = GENERATE(range(1, 65, 1));
  // std::uint64_t stride = 1;
  std::uint64_t threadsPerBlock = 32;
  std::uint64_t numBlocks = 1;
  std::uint64_t iterations = 300;
  std::uint64_t memorySize = threadsPerBlock * sizeof(int) * stride + iterations * sizeof(int);
  std::uint64_t numberOfInts = memorySize / sizeof(int);
  const int numberOfRuns = 10;

  CAPTURE(stride);
  CAPTURE(threadsPerBlock);
  // CAPTURE(numBlocks);
  CAPTURE(iterations);
  SUCCEED();
  std::uint64_t *dClocks = nullptr;
  REQUIRE(cudaMalloc(&dClocks, threadsPerBlock * sizeof(std::uint64_t)) == cudaSuccess);

  for(std::size_t i = 0; i < numberOfRuns; ++i)
  {
    readSharedMemory<<<numBlocks, threadsPerBlock, memorySize>>>(iterations, stride, dClocks);
  }
  // warmup - no need for device synchronize, as we use device clocks
  
  readSharedMemory<<<numBlocks, threadsPerBlock, memorySize>>>(iterations, stride, dClocks);
  CHECK(cudaDeviceSynchronize() == cudaSuccess);

  std::vector<std::uint64_t> clocks(threadsPerBlock);
  CHECK(cudaMemcpy(clocks.data(), dClocks, threadsPerBlock * sizeof(std::uint64_t), cudaMemcpyDeviceToHost) == cudaSuccess);

  std::uint64_t avgClock = 0;
  for(size_t i = 0; i < threadsPerBlock; ++i)
  {
    avgClock += clocks[i];
  }
  avgClock /= threadsPerBlock;
  std::cout << "strided shared read\t" << avgClock << "\tstride := " << stride << "\titerations := " << iterations << std::endl;

  cudaFree(dClocks);
}
