// run with: ./bin/4_2 -s -r csv
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

//count: number of elements per thread Block
__global__ void
memCopySharedToGlobal(int* globalMem, int count)
{
    extern __shared__ int sharedMem [];
    int const globalOffset = blockIdx.x * count;
    int const sharedOffset = threadIdx.x;
    int currElement = sharedOffset;
    while (currElement < count) {
        globalMem[currElement + globalOffset] = sharedMem[currElement];
        currElement += blockDim.x;
    }
}

__global__ void
memCopyGlobalToShared(int* globalMem, int count)
{
    extern __shared__ int sharedMem [];
    int const globalOffset = blockIdx.x * count;
    int const sharedOffset = threadIdx.x;
    int currElement = sharedOffset;
    while (currElement < count) {
        sharedMem[currElement] = globalMem[currElement + globalOffset];
        currElement += blockDim.x;
    }
}

__global__ void
readSharedMemory(int count)
{
    extern __shared__ int sharedMem [];
    int currElement = threadIdx.x;
    int max = 0;
    while (currElement < count) {
        int currValue = sharedMem[currElement];
        max = max < currValue ? currValue : max;
        currElement += blockDim.x;
    }
    if(max == threadIdx.x){
        //this should prevent the compiler from optimizing this kernel to nothing while also not adding a lot of memory accesses
        sharedMem[threadIdx.x] = max;
    }
}

__global__ void
writeSharedMemory(int count)
{
    extern __shared__ int sharedMem [];
    int currElement = threadIdx.x;
    while (currElement < count) {
        //local variables are stored in register memory and we only need a little bit of data from each thread to fill the shared memory
        sharedMem[currElement] = currElement;
        currElement += blockDim.x;
    }
}

SCENARIO("4.2a shared basis")
{
    std::uint64_t threadsPerBlock = 32;
    std::uint64_t numBlocks = GENERATE(logRange(1,65537,2));
    //std::uint64_t memorySize = GENERATE(range(1000,49000,1000));
    std::uint64_t memorySize = 48000;
    std::uint64_t numberOfInts = memorySize / sizeof(int);
    const int numberOfRuns = 10;
    std::uint64_t totalMemorySize = memorySize * numBlocks * numberOfRuns;

    CAPTURE(threadsPerBlock);
    CAPTURE(totalMemorySize);
    CAPTURE(numBlocks);
    CAPTURE(memorySize);
    cudaError_t cudaError;

    int* globalMem = nullptr;
    cudaError = cudaMalloc(&globalMem, totalMemorySize);
    REQUIRE(cudaError == cudaSuccess);
    REQUIRE(globalMem != nullptr);


    BENCHMARK("global to shared")
    {
        for(int i = 0; i < numberOfRuns; i++) //looping like this 
            memCopyGlobalToShared<<<numBlocks,threadsPerBlock,memorySize>>>(globalMem, numberOfInts);
        cudaError = cudaDeviceSynchronize();
    };

    CHECK(cudaError == cudaSuccess);

    BENCHMARK("shared to global")
    {
        for(int i = 0; i < numberOfRuns; i++)
            memCopySharedToGlobal<<<numBlocks,threadsPerBlock,memorySize>>>(globalMem,numberOfInts);
        cudaError = cudaDeviceSynchronize();
    };
    CHECK (cudaError == cudaSuccess);

    cudaError = cudaFree(globalMem);
    CHECK (cudaError == cudaSuccess);   
}

SCENARIO("4.2b shared basis registers")
{
    std::uint64_t threadsPerBlock = 1024;
    std::uint64_t numBlocks = 256;
    std::uint64_t memorySize = GENERATE(range(1000,49000,1000));
    //std::uint64_t memorySize = 48000;
    std::uint64_t numberOfInts = memorySize / sizeof(int);
    const int numberOfRuns = 10;
    std::uint64_t totalMemorySize = memorySize * numBlocks * numberOfRuns;

    CAPTURE(threadsPerBlock);
    CAPTURE(totalMemorySize);
    CAPTURE(numBlocks);
    CAPTURE(memorySize);
    SUCCEED();
    cudaError_t cudaError;

    BENCHMARK("register to shared")
    {
        for(int i = 0; i < numberOfRuns; i++)
            writeSharedMemory<<<numBlocks,threadsPerBlock,memorySize>>>(numberOfInts);
        cudaError = cudaDeviceSynchronize();
    };

    CHECK(cudaError == cudaSuccess);

    BENCHMARK("shared to register")
    {
        for(int i = 0; i < numberOfRuns; i++)
            readSharedMemory<<<numBlocks,threadsPerBlock,memorySize>>>(numberOfInts);
        cudaError = cudaDeviceSynchronize();
    };
    CHECK (cudaError == cudaSuccess);
}