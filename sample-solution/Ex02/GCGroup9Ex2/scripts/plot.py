import matplotlib.pyplot as plt
import matplotlib as mpl
import fire
import pandas
import numpy as np
import os, sys

def set_markers_for_axis(ax):
    markers = ([item[0] for item in mpl.markers.MarkerStyle.markers.items() if 
        item[1] != 'nothing' and not item[1].startswith('tick') 
        and not item[1].startswith('caret') and not item[0] == ','])
    for i, line in enumerate(ax.get_lines()):
        print(markers[i])
        line.set_marker(markers[i])

class cli:
    @staticmethod
    def plot_catch(csv : str, index_col : str = "N", xscale : str = 'linear', yscale : str = 'linear', kind : str = 'line', show_all_ticks : bool = False, fig_size = (5,5), filetype : str = "pdf"):
        """
        Plots a Catch2 benchmark csv file.
        
        :param file: Input file.
        :param index_col: The name of the captured C++ variable to use for the x axis. Can also be a list (e.g. "['numBlocks','threadsPerBlock']").
        :param xscale: linear or log (or whatever matplotlib allows)
        :param yscale: linear or log (or whatever matplotlib allows)
        :param kind: line (or whatever matplotlib allows)
        :param filetype: pdf, png, svg (or whatever matplotlibg allows)
        """
        df = pandas.read_csv(csv, sep="\t", header=None)
        name = {}
        def extract_value(value, col):
            vals = value.split(" := ")
            if len(vals) > 1:
                name[col] = vals[0]
                if "(" in vals[1]: # get rid of (0x..)
                    vals[1] = vals[1][0:vals[1].index("(")]
                try:
                    return int(vals[1])
                except ValueError:
                    return vals[1]

            return vals[0]

        for i in range(2, len(df.columns)):
            df[i] = df[i].apply(extract_value, True, (i,))

        name[0] = "benchmark"
        name[1] = "time"
        df.rename(columns=name, inplace=True)
        df = df.pivot_table(index=index_col, columns='benchmark', values='time')

        plt.figure(figsize=fig_size)
        ax = plt.gca()
        ax.set_xscale(xscale)
        ax.set_yscale(yscale)
        ax.set_ylabel("time [ns]")
        df.plot(kind=kind, ax=ax)

        if show_all_ticks:
            tick_names = df.index.to_list()
            ax.set_xticks(np.arange(len(tick_names)))
            ax.set_xticklabels(df.index.to_list(), rotation=90)
            # fig.subplots_adjust(bottom=0.2)
        plt.tight_layout()
        
        if kind == 'line':
            set_markers_for_axis(ax)

        plt.savefig(csv + "." + filetype, format=filetype)

    @staticmethod
    def plot_minstructor(csv : str, index_col : str = "N", xscale : str = 'linear', yscale : str = 'linear', kind : str = 'line', filetype : str = "pdf"):
        """
        Plots a minstructor benchmark csv file.

        :param file: Input file.
        :param filetype: pdf, png, svg (or whatever matplotlibg allows
        """
        df = pandas.read_csv(csv, index_col=index_col)
        df = df.drop(columns="data-file-path")

        ax = plt.gca()
        ax.set_xscale(xscale)
        ax.set_yscale(yscale)
        df.plot(kind=kind, ax=ax)
        
        if kind == 'line':
            set_markers_for_axis(ax)

        plt.savefig(csv + "." + filetype, format=filetype)

if __name__ == "__main__":
    fire.Fire(cli)