#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -o ex2_1out.txt
#SBATCH -p bench

bin/nullKernelAsync -s -r csv --benchmark-samples=10
