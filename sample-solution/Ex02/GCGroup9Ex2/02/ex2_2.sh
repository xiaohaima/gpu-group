#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -o ex2_2out.txt
#SBATCH -p bench

bin/breakEvenKernel -s -r csv --benchmark-samples=10
