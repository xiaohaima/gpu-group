#!/usr/bin/env bash
#SBATCH --gres=gpu:1
#SBATCH -o ex2_3out.txt
#SBATCH -p bench

bin/PCIe_Data_Movements -s -r csv --benchmark-samples=10
