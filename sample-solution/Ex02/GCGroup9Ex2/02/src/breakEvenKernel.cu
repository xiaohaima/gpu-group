#include <chrono>
#include <iostream>
#include <array>
#include <numeric>

__device__ clock_t a = 0;

__global__
void
BusyWaitKernel(clock_t cycles)
{
    auto start = clock64();
    auto stop = clock64();
    while ((stop - start) < cycles ) {
        stop = clock();
        if (threadIdx.x == 42)
            a = (stop - start);
    }

}

using Clock = std::chrono::high_resolution_clock;
using DNanoseconds = std::chrono::duration<double, std::nano>;

int main () {

    BusyWaitKernel<<<1,1>>>(100);
    BusyWaitKernel<<<1,1>>>(100);
    cudaDeviceSynchronize ();
    constexpr int runs = 1000;

    for (int cycles = 0; cycles <= 2'000; cycles += 50){
        std::array<DNanoseconds, runs> times;
        for (auto& time : times){
            auto start = Clock::now();
            BusyWaitKernel<<<1,1>>>(cycles);
            cudaDeviceSynchronize();
            auto end = Clock::now();
            time = end - start;
        }
        DNanoseconds averageTime = std::accumulate(times.begin(), times.end(),DNanoseconds::zero())/times.size();

        std::cout << "launch time\t" << averageTime.count() << "\tcycles := " << cycles << "\n";
    }
}