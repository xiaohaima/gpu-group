// run with: ./bin/nullKernelAsync -s -r csv --benchmark-samples=10
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

__global__
void
NullKernel()
{
}

SCENARIO("2.1 raw kernel startup time")
{
    // generate runs with 1, 4, 16, .. 16384 numBlocks
    std::uint64_t numBlocks = GENERATE(logRange(1, 32768, 4));
    std::uint64_t threadsPerBlock = GENERATE(logRange(1, 2048, 4));
 
    // capture values for plotting
    CAPTURE(numBlocks);
    CAPTURE(threadsPerBlock);
    SUCCEED(); // required to print the captured values

    BENCHMARK("async launch time")
    {
        NullKernel<<<numBlocks,threadsPerBlock>>>();
    };
    // synchronize, so the next benchmark is unaffected
    auto cudaError = cudaDeviceSynchronize();
    //check if the kernel was actually sucessfully executed
    CHECK (cudaError == cudaSuccess);

    BENCHMARK("sync launch time")
    {
        NullKernel<<<numBlocks,threadsPerBlock>>>();
        cudaError = cudaDeviceSynchronize();
    };
    CHECK (cudaError == cudaSuccess);
}
