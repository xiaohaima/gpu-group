// run with: ./bin/PCIe_Data_Movements -s -r csv --benchmark-samples=10
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

SCENARIO("2.3 PCIe Data Movement")
{
    // generate runs with 1024, 2048, ... 1G
    std::uint64_t allocated_memory = GENERATE(logRange(1024ULL, 2*1024*1024*1024ULL, 2ULL));
 
    // capture values for plotting
    CAPTURE(allocated_memory);
    SUCCEED(); // required to print the captured values

    //Pointer to host & device arrays
    float *hmem_paged, *dmem_paged, *hmem_pinned, *dmem_pinned;

    //Allocate CPU memory
    hmem_paged = (float *)malloc(allocated_memory*sizeof(char));                                    //Host, paged
    cudaMallocHost((void **) &hmem_pinned, allocated_memory*sizeof(char) );                         //Host, pinned

    cudaMalloc((void **) &dmem_pinned, allocated_memory*sizeof(char));                              //Device, paged
    cudaMalloc((void **) &dmem_paged, allocated_memory*sizeof(char));                               //Device, paged

    BENCHMARK("H2D pageable memory")
    {
        //Transfer data from host to device
        cudaMemcpy (dmem_paged, hmem_paged, allocated_memory*sizeof(char), cudaMemcpyHostToDevice);
    };
    // synchronize, so the next benchmark is unaffected
    auto cudaError = cudaDeviceSynchronize();
    //check if the kernel was actually sucessfully executed
    CHECK (cudaError == cudaSuccess);

    BENCHMARK("D2H pageable memory")
    {
        //Transfer data back from device to host
        cudaMemcpy (hmem_paged, dmem_paged, allocated_memory*sizeof(char), cudaMemcpyDeviceToHost);
    };
    CHECK (cudaError == cudaSuccess);

    free(hmem_paged);   //Free host buffer

    BENCHMARK("H2D pinned memory")
    {
        //Transfer data from host to device
        cudaMemcpy (dmem_pinned, hmem_pinned, allocated_memory*sizeof(char), cudaMemcpyHostToDevice);

    };
    CHECK (cudaError == cudaSuccess);

    BENCHMARK("D2H pinned memory")
    {
        //Transfer data from host to device
        cudaMemcpy (hmem_pinned, dmem_pinned, allocated_memory*sizeof(char), cudaMemcpyDeviceToHost);

    };
    CHECK (cudaError == cudaSuccess);

    cudaFree(hmem_pinned);   //Free host buffer
    cudaFree(dmem_pinned);
    cudaFree(dmem_paged);
}
