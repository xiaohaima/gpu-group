

float performStep(const float* before, float* after, int size);
void writeToFile(const float * grid, const char * name, int size);
void prepareGrid(float* data, int size);