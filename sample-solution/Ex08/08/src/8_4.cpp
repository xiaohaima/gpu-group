#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

#include <iostream>
#include <vector>

#include "heatRelaxation.hpp"

namespace org
{

constexpr float phi = 24.f/100.f;

float performStep(const float* restrict before, float* restrict after, int size) {
    //before and after must point to an array of length size²
    float largestChange = 0;
    //the values on the border stay fixed

    for(int j = 1; j < size-1; j++){
        #pragma acc kernels loop reduction(max:largestChange)
        for(int i = 1; i < size-1; i++){

            const int here = j*size + i;
            const int left = j*size + (i-1);
            const int up   = (j+1)*size + i;
            const int right= j*size + (i + 1);
            const int down = (j-1)*size + i;

            after[here] = before[here] + phi * (-4.f * before[here] + before[left] + before[up] + before[right] + before[down]);

            const float change = std::abs(before[here] - after[here]);
            largestChange = std::max(change,largestChange);
        }
    }
    return largestChange;
}

}

namespace acc {
constexpr float phi = 24.f/100.f;

template<size_t VL = 128>
float performStep(const float* restrict before, float* restrict after, int size) {
    //before and after must point to an array of length size²
    float largestChange = 0;
    //the values on the border stay fixed

    #pragma acc data present(before[0:size*size], after[0:size*size])
    {
    #pragma acc parallel loop collapse(2) cache(before) reduction(max:largestChange) vector_length(VL)
    for(int j = 1; j < size-1; j++){
        for(int i = 1; i < size-1; i++){

            const int here = j*size + i;
            const int left = j*size + (i-1);
            const int up   = (j+1)*size + i;
            const int right= j*size + (i + 1);
            const int down = (j-1)*size + i;

            after[here] = before[here] + phi * (-4.f * before[here] + before[left] + before[up] + before[right] + before[down]);

            const float change = std::abs(before[here] - after[here]);
            largestChange = std::max(change, largestChange);
        }
    }
    }
    return largestChange;
}

void prepareGrid(float* data, int size) {

    #pragma acc kernels loop independent collapse(2) copy(data[:size*size])
    for(int j = 0; j < size; j++){
        for(int i = 0; i < size; i++){
            data[j*size + i] =
                (i == 0 && j < (3*size)/4 && j > size/4) ? 127: //heat saurce at the top
                (i < (3*size)/4 && i > size/4 && j < (3*size)/4 && j > size/4) ? 100: //warm square in the middle
                0; //everywhere else
        }
    }
}

void writeToFile(const float * grid, const char * name, int size) {
    FILE * pFile;

    pFile = fopen (name,"w");
    int i,j;

    fprintf (pFile, "P2 %d %d %d\n", size, size, 127);

    for(i = 0; i<size; i++)
    {
        for(j = 0; j<size; j++)
        {
            fprintf (pFile, "%d ", (int) grid[j*size + i]);
        }
        fprintf (pFile, "\n");
    }

    fclose (pFile);

    return;
}
}

TEMPLATE_TEST_CASE_SIG("8.4 optimized OpenACC heat relaxation", "[test]", ((size_t VL), VL), 128ULL, 256ULL, 512ULL, 1024ULL)
{
    const auto size = GENERATE(64ULL, 128ULL, 256ULL, 512ULL);
    CAPTURE(size);
    CAPTURE(VL);

    std::vector<float> A(size * size);
    std::vector<float> B(size * size);

    acc::prepareGrid(A.data(), size);
    acc::prepareGrid(B.data(), size);

    float change = 1;

    std::vector<float> Ao(size * size);
    std::vector<float> Bo(size * size);

    acc::prepareGrid(Ao.data(), size);
    acc::prepareGrid(Bo.data(), size);

    float changeO = 1;

    std::vector<float> As(size * size);
    std::vector<float> Bs(size * size);

    prepareGrid(As.data(), size);
    prepareGrid(Bs.data(), size);

    float changeS = 1;
    {
    auto *a = A.data();
    auto *b = B.data();
    const std::uint64_t sz = size*size;
    #pragma acc data copy(a[:sz], b[:sz])
    {
    for (int i = 0; change > 10e-3; i++)
    {
        if (i % 2 == 0)
        {
          change = acc::performStep<VL>(a, b, size);
          changeS = performStep(As.data(), Bs.data(), size);
          changeO = org::performStep(Ao.data(), Bo.data(), size);
        }
        else
        {
          change = acc::performStep<VL>(b, a, size);
          changeS = performStep(Bs.data(), As.data(), size);
          changeO = org::performStep(Bo.data(), Ao.data(), size);
        }
        CAPTURE(i);
        CHECK(change == Approx(changeS));
        CHECK(changeO == Approx(changeS));
    }
    }
    }
    CHECK_THAT(A, Catch::Matchers::Approx(As));
    CHECK_THAT(Ao, Catch::Matchers::Approx(As));
    acc::writeToFile(A.data(), "final.pgm", size);
}

SCENARIO("Benchmark org", "[!benchmark][org]")
{
    const auto size = GENERATE(64ULL, 128ULL, 256ULL, 512ULL);
    const auto threadsPerBlock = 128;
    CAPTURE(size);
    CAPTURE(threadsPerBlock);
    SUCCEED();

    // allocate some extra..
    std::vector<float> A(size * size * 10);
    std::vector<float> B(size * size * 10);

    BENCHMARK_ADVANCED("perform org relaxation")(Catch::Benchmark::Chronometer meter)
    {
      if(meter.runs() > 10)
        throw std::runtime_error{"unexpected many runs"};

      for(int i = 0; i < meter.runs(); ++i)
      {
        acc::prepareGrid(A.data() + i * size * size, size);
        acc::prepareGrid(B.data() + i * size * size, size);
      }

      meter.measure([&A, &B, size](int i)
      {
        float change = 1;
        auto *a = A.data() + i * size * size;
        auto *b = B.data() + i * size * size;
        for (int i = 0; change > 10e-3; i++)
        {
            if (i % 2 == 0)
                change = org::performStep(a, b, size);
            else
                change = org::performStep(b, a, size);
        }
        return change;
      });
    };

}

TEMPLATE_TEST_CASE_SIG("Benchmark opt", "[!benchmark][opt]", ((size_t VL), VL), 128ULL, 256ULL, 512ULL, 1024ULL)
{
    const auto size = GENERATE(64ULL, 128ULL, 256ULL, 512ULL);
    const auto threadsPerBlock = VL;
    CAPTURE(size);
    CAPTURE(threadsPerBlock);
    SUCCEED();

    std::vector<float> A(size * size * 50);
    std::vector<float> B(size * size * 50);

    BENCHMARK_ADVANCED("perform opt relaxation")(Catch::Benchmark::Chronometer meter)
    {
      if(meter.runs() > 50)
        throw std::runtime_error{"unexpected many runs"};

      for(int i = 0; i < meter.runs(); ++i)
      {
        acc::prepareGrid(A.data() + i * size * size, size);
        acc::prepareGrid(B.data() + i * size * size, size);
      }

      meter.measure([&A, &B, size](int i)
      {
        float change = 1;
        auto *a = A.data() + i * size * size;
        auto *b = B.data() + i * size * size;
        const std::uint64_t sz = size*size;
        #pragma acc data copy(a[:sz], b[:sz])
        {
          change = 1;
          int i = 0;

          for(; change > 10e-3; i++)
          {
              if (i % 2 == 0)
                  change = acc::performStep<VL>(a, b, size);
              else
                  change = acc::performStep<VL>(b, a, size);
          }
        }
        return change;
      });
    };
}
