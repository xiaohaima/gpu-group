
#include <iostream>
#include <vector>

constexpr float phi = 24.f/100.f;

float performStep(const float* restrict before, float* restrict after, int size) {
    //before and after must point to an array of length size²
    float largestChange = 0;
    //the values on the border stay fixed
    
    for(int j = 1; j < size-1; j++){
        #pragma acc kernels loop reduction(max:largestChange)
        for(int i = 1; i < size-1; i++){

            const int here = j*size + i;
            const int left = j*size + (i-1);
            const int up   = (j+1)*size + i;
            const int right= j*size + (i + 1);
            const int down = (j-1)*size + i;

            after[here] = before[here] + phi * (-4.f * before[here] + before[left] + before[up] + before[right] + before[down]);

            const float change = std::abs(before[here] - after[here]);
            largestChange = std::max(change,largestChange);
        }
    }
    return largestChange;
}

void prepareGrid(float* data, int size) {
    
    for(int j = 0; j < size; j++){
    #pragma acc kernels
    {
        for(int i = 0; i < size; i++){
            data[j*size + i] = 
                (i == 0 && j < (3*size)/4 && j > size/4) ? 127: //heat saurce at the top
                (i < (3*size)/4 && i > size/4 && j < (3*size)/4 && j > size/4) ? 100: //warm square in the middle
                0; //everywhere else
        }
    }
    }
}

void writeToFile(const float * grid, const char * name, int size) { 
    FILE * pFile;
    
    pFile = fopen (name,"w");
    int i,j;
    
    fprintf (pFile, "P2 %d %d %d\n", size, size, 127);
    
    for(i = 0; i<size; i++)
    {
        for(j = 0; j<size; j++)
        {
            fprintf (pFile, "%d ", (int) grid[j*size + i]);
        }
        fprintf (pFile, "\n");
    }
    
    fclose (pFile);
    
    return;
}

int main(int argc, char **argv)
{

    if (argc < 2)
    {
        std::cout << "usage: " << argv[0] << " <size>\n";
        return -1;
    }

    const int size = std::stoi(argv[1]);

    if (size < 3)
    {
        std::cout << "size is too small\n";
        return -1;
    }

    std::vector<float> A(size * size);
    std::vector<float> B(size * size);

    prepareGrid(A.data(), size);
    prepareGrid(B.data(), size);

    float change = 1;
    
    for (int i = 0; change > 10e-3; i++)
    {
        if (i % 2 == 0)
            change = performStep(A.data(), B.data(), size);
        else
            change = performStep(B.data(), A.data(), size);
        std::cout << change << "\titeration := " << i << "\n";
        if (i == 100)
            writeToFile(A.data(), "100Iterations.pgm", size);
        }
    writeToFile(A.data(), "final.pgm", size);
}
