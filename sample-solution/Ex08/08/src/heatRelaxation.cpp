#include "heatRelaxation.hpp"

#include <numeric>
#include <algorithm>

#include <cstdio>
#include <cstdlib>

constexpr float phi = 24.f/100.f;

float performStep(const float* before, float* after, int size) {
    //before and after must point to an array of length size²
    float largestChange = 0;
    //the values on the border stay fixed
    for(int j = 1; j < size-1; j++){
        for(int i = 1; i < size-1; i++){

            const int here = j*size + i;
            const int left = j*size + (i-1);
            const int up   = (j+1)*size + i;
            const int right= j*size + (i + 1);
            const int down = (j-1)*size + i;

            after[here] = before[here] + phi * (-4.f * before[here] + before[left] + before[up] + before[right] + before[down]);

            const float change = std::abs(before[here] - after[here]);
            largestChange = std::max(change,largestChange);
        }
    }
    return largestChange;
}

void prepareGrid(float* data, int size) {
    for(int j = 0; j < size; j++){
        for(int i = 0; i < size; i++){
            data[j*size + i] = 
                (i == 0 && j < (3*size)/4 && j > size/4) ? 127: //heat saurce at the top
                (i < (3*size)/4 && i > size/4 && j < (3*size)/4 && j > size/4) ? 100: //warm square in the middle
                0; //everywhere else
        }
    }
}

void writeToFile(const float * grid, const char * name, int size) { 
    FILE * pFile;
    
    pFile = fopen (name,"w");
    int i,j;
    
    fprintf (pFile, "P2 %d %d %d\n", size, size, 127);
    
    for(i = 0; i<size; i++)
    {
        for(j = 0; j<size; j++)
        {
            fprintf (pFile, "%d ", (int) grid[j*size + i]);
        }
        fprintf (pFile, "\n");
    }
    
    fclose (pFile);
    
    return;
}