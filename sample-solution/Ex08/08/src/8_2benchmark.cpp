#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <csv_reporter.hpp>
#include <logrange_generator.hpp>

#include "heatRelaxation.hpp"

SCENARIO("8.2 heat relaxation serial CPU", "[cpu]")
{

    const int size = GENERATE(logRange(128, (1ULL << 10) + 1ULL, 2));
    const int iterations = 1000;

    std::vector<float> A(size * size);
    std::vector<float> B(size * size);

    prepareGrid(A.data(), size);
    prepareGrid(B.data(), size);

    BENCHMARK("8.2 serial stencil")
    {
        for (int i = 0; i < iterations; i++)
        {
            if (i % 2 == 0)
                performStep(A.data(), B.data(), size);
            else
                performStep(B.data(), A.data(), size);
        }
    };
}