
#include "heatRelaxation.hpp"
#include <iostream>
#include <vector>

int main(int argc, char **argv)
{

    if (argc < 2)
    {
        std::cout << "usage: " << argv[0] << " <size>\n";
        return -1;
    }

    const int size = std::stoi(argv[1]);

    if (size < 3)
    {
        std::cout << "size is too small\n";
        return -1;
    }

    std::vector<float> A(size * size);
    std::vector<float> B(size * size);

    prepareGrid(A.data(), size);
    prepareGrid(B.data(), size);

    float change = 1;

    for (int i = 0; change > 10e-3; i++)
    {
        if (i % 2 == 0)
            change = performStep(A.data(), B.data(), size);
        else
            change = performStep(B.data(), A.data(), size);
        std::cout << change << "\titeration := " << i << "\n";
        if (i == 100)
            writeToFile(A.data(), "100Iterations.pgm", size);
    }
    writeToFile(A.data(), "final.pgm", size);
}